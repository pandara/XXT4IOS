//
//  XXTTableCell.m
//  XXTForIOS
//
//  Created by pandara on 13-9-13.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTTableCell.h"
#import "UIImageView+JMImageCache.h"
#import <QuartzCore/QuartzCore.h>

@implementation XXTTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"XXTTableCell" owner:self options:nil] objectAtIndex:0];
        
        self.theImageView.layer.cornerRadius = 5.0f;
        
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = color(200, 200, 200, 1).CGColor;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setStyle:(XXTTableCellStyle)style
{
    switch (style) {
        case XXTTableCellStyleNoTime:
        {
            self.theTimeLabel.hidden = YES;
        }
            break;
        case XXTTableCellStyleWithTime:
        {
            self.theTimeLabel.hidden = NO;
        }
            break;
        default:
            break;
    }
}

- (void)setImageURLStr:(NSString *)imageURLStr
                 title:(NSString *)title
              subTitle:(NSString *)subTitle
{
    [self setImageURLStr:imageURLStr title:title subTitle:subTitle dateStr:@""];
}

- (void)setImageURLStr:(NSString *)imageURLStr
                 title:(NSString *)title
              subTitle:(NSString *)subTitle
               dateStr:(NSString *)dateStr
{
    if (_theImageView.image == nil) {
        [self.theImageView setImageWithURL:[NSURL URLWithString:imageURLStr] placeholder:[UIImage imageNamed:@"avatar_default_rect.png"]];
    } else {
        [self.theImageView setImageWithURL:[NSURL URLWithString:imageURLStr] placeholder:_theImageView.image];
    }

    self.theTitleLabel.text = title;
    
    self.theSubTitleLabel.text = subTitle;
    
    self.theTimeLabel.text = dateStr;
}

@end
