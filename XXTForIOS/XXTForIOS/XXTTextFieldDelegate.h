//
//  XXTTextFieldDelegate.h
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XXTTextField;
@protocol XXTTextFieldDelegate <NSObject>

- (void)textFieldDidBeginEditing:(XXTTextField *)textField;
- (void)textFieldDidEndEditing:(XXTTextField *)textField;

@end
