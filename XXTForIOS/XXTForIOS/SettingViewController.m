//
//  SettingViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SettingViewController.h"
#import "FeedBackViewController.h"
#import "AboutUsViewController.h"
#import "BlocksKit.h"
#import "LoginViewController.h"
#import "SBNavigationViewController.h"
#import "SVProgressHUD.h"
#import "JMImageCache.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"设置"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressFeedBackButton:(id)sender {
    FeedBackViewController *con = [[FeedBackViewController alloc] initWithNibName:@"FeedBackViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

- (IBAction)pressUpdateViewController:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"更新检测" message:@"当前版本(v1.0)为最新" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
}

- (IBAction)pressAboutUsButton:(id)sender {
    AboutUsViewController *con = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

- (IBAction)pressLogoutButton:(id)sender {
    [[XXTToolKit getAppdelegate] setIsReLogin:YES];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定要退出?"];
    [alertView addButtonWithTitle:@"取消"];
    [alertView addButtonWithTitle:@"确定" handler:^{
        
        [[JMImageCache sharedCache] removeAllObjects];
        [XXTToolKit setCalendarArray:@[]];
        [XXTToolKit deleteDataFromFriendCache];
        [XXTToolKit deleteDataFromChatListCacheWithErrorBlock:nil completeBlock:nil];
        [XXTToolKit deleteDataFromAllChatListCache];
        [XXTToolKit setFriendFeedList:@[]];
        [XXTToolKit setFriendInfoList:@[]];
        [XXTToolKit setHasLogined:NO];
        [XXTToolKit setMyFeedList:@[]];
        [XXTToolKit saveUserData:@{}];
        [XXTToolKit setPasswd:@""];
        [XXTToolKit setMAuth:@""];
        [XXTToolKit setName:@""];
        
        SBNavigationViewController *navCon = [[SBNavigationViewController alloc] init];
        LoginViewController *con = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        [navCon pushViewController:con animated:NO];
        PRESENT_VIEWCONTROLLER(navCon, [[XXTToolKit getAppdelegate] navigationController], NO);
        
        [[[XXTToolKit getAppdelegate] navigationController] popViewControllerAnimated:NO];
    }];
    [alertView show];
}

- (IBAction)pressCleanChatCacheButton:(id)sender {
    CGFloat fileSize = [XXTToolKit getSizeOfChatListCache];
    fileSize = fileSize / 1024.0 / 1024.0;
    NSString *fileSizeStr = [NSString stringWithFormat:@"%.2f", fileSize];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"聊天记录缓存大小为%@MB,确定要清除吗？", fileSizeStr]];
    [alertView addButtonWithTitle:@"取消"];
    [alertView addButtonWithTitle:@"确定" handler:^{
        [XXTToolKit deleteDataFromChatListCacheWithErrorBlock:^(NSString *str) {
            [SVProgressHUD showErrorWithStatus:str];
        } completeBlock:^{
            [SVProgressHUD showSuccessWithStatus:@"删除聊天缓存成功"];
        }];
    }];
    [alertView show];
}

- (void)viewDidUnload {
    [self setClearButton:nil];
    [super viewDidUnload];
}



@end










