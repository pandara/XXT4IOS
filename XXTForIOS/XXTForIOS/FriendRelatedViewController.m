//
//  FriendRelatedViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-26.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FriendRelatedViewController.h"

@interface FriendRelatedViewController ()

@end

@implementation FriendRelatedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:NOTIFI_RELOAD_FRIENDINFORELATED_CON object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFI_RELOAD_FRIENDINFORELATED_CON object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
