//
//  SwipeDownResignViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-29.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SwipeDownResignViewController.h"
#import "XXTTextView.h"

@interface SwipeDownResignViewController ()

@end

@implementation SwipeDownResignViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeTheView:)];
    gesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:gesture];
}

- (void)swipeTheView:(UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%@", [self.view subviews]);
    for (UIView *subView in [self.view subviews]) {
        if ([subView isKindOfClass:[XXTTextView class]]) {
            if ([subView isFirstResponder]) {
                [subView resignFirstResponder];
                return;
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
