//
//  AppDelegate.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "AppDelegate.h"
#import "ScholatViewController.h"
#import "FriendViewController.h"
#import "FeedViewController.h"
#import "SettingViewController.h"
#import "DCIntrospect.h"
#import "LoadingView.h"
#import "BlocksKit.h"
#import "BootViewController.h"
#import "SVProgressHUD.h"
#import "CalendarFireViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (launchOptions) {
        UILocalNotification *localNotificaiton = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        if (nilObject(localNotificaiton)) {
            _localNotificaiton = localNotificaiton;
        }
    }
    
    _appDict = [[NSMutableDictionary alloc] init];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //navigationController
    self.navigationController = [[SBNavigationViewController alloc] init];
    self.window.rootViewController = self.navigationController;
    
    [self.window makeKeyAndVisible];
    
    LoadingView *loadView = [[LoadingView alloc] init];
    [loadView beginAnimation];
    [self.window addSubview:loadView];
    _loadView = loadView;
    
    //tabBarController
    if (![XXTToolKit isNotFirstOpenApp]) {
        [self performBlock:^(id sender) {
            [_loadView stopAnimation];
        } afterDelay:2.0f];
        BootViewController *bootViewController = [[BootViewController alloc] initWithNibName:@"BootViewController" bundle:nil];
        
        SBNavigationViewController *presentedNavCon = [[SBNavigationViewController alloc] init];
        [presentedNavCon pushViewController:bootViewController animated:NO];
        
        PRESENT_VIEWCONTROLLER(presentedNavCon, self.navigationController, NO);
        [XXTToolKit setNotFirstOpenApp:YES];
    } else {
        if (![XXTToolKit getHasLogined]) {
            
            [self performBlock:^(id sender) {
                [_loadView stopAnimation];
            } afterDelay:2.0f];
            
            [self presentLoginViewController];
        } else {
            [self performBlock:^(id sender) {
                [_loadView stopAnimation];
            } afterDelay:2.0f];
            
            [[[XXTToolKit getAppdelegate] navigationController] pushViewController:[[XXTToolKit getAppdelegate] getMainTabBarController] animated:NO];
        }
    }
    
#if TARGET_IPHONE_SIMULATOR
    [[DCIntrospect sharedIntrospector] start];
#endif 
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self handleLocalNotificaiton:notification];
}

- (void)handleLocalNotificaiton:(UILocalNotification *)notification
{
    CalendarFireViewController *con = [[CalendarFireViewController alloc] initWithNibName:@"CalendarFireViewController" bundle:nil];
    con.notification = notification;
    PRESENT_VIEWCONTROLLER(con, self.navigationController, YES);
}

- (void)presentLoginViewController
{
    self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    SBNavigationViewController *presentedNavCon = [[SBNavigationViewController alloc] init];
    [presentedNavCon pushViewController:self.loginViewController animated:NO];
    
    PRESENT_VIEWCONTROLLER(presentedNavCon, self.navigationController, NO);
}

- (SBTabBarController *)getMainTabBarController
{
    ScholatViewController *scholatViewController = [[ScholatViewController alloc] initWithNibName:@"ScholatViewController" bundle:nil];
    FriendViewController *friendViewController = [[FriendViewController alloc] initWithNibName:@"FriendViewController" bundle:nil];
    FeedViewController *feedViewController = [[FeedViewController alloc] initWithNibName:@"FeedViewController" bundle:nil];
    SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    
    SBTabBarController *tabBarController = [[SBTabBarController alloc] initWithNibName:@"SBTabBarController" bundle:nil];
    tabBarController.localNotificaiton = _localNotificaiton;
    [tabBarController setViewControllers:@[scholatViewController, friendViewController, feedViewController, settingViewController]];
    
    return tabBarController;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
