//
//  LoginSubViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "LoginSubViewController.h"
#import "XXTHttpClient.h"
#import "SVProgressHUD.h"
#import "BlocksKit.h"

@interface LoginSubViewController ()

@end

@implementation LoginSubViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *loginbtnImage;
    UIImage *loginBtnImageHL;
    
    const CGFloat resizeImageInset = 10;
    const CGFloat resizeImageInsetLeft = 10;
    
    if (DEVICE_OS_VERSION < 6.0f) {
        loginbtnImage = [[UIImage imageNamed:@"btn_green.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
        loginBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
    } else if (DEVICE_OS_VERSION >= 6.0f) {
        loginbtnImage = [[UIImage imageNamed:@"btn_green.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
        loginBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
    }
    
    [self.loginButton setBackgroundImage:loginbtnImage forState:UIControlStateNormal];
    [self.loginButton setBackgroundImage:loginBtnImageHL forState:UIControlStateHighlighted];
    
    [self.accountTextField becomeFirstResponder];
    
    self.navigationItem.title = @"登录";
    self.navigationItem.backBarButtonItem.title = @"返回";
    
//    self.accountTextField.text = @"Pandara";
//    self.passwdTextField.text = @"1";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.5f animations:^{
        self.navigationController.navigationBarHidden = NO;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLoginButton:nil];
    [self setAccountTextField:nil];
    [self setPasswdTextField:nil];
    [super viewDidUnload];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.accountTextField resignFirstResponder];
    [self.passwdTextField resignFirstResponder];
}
- (IBAction)pressRestPasswdBtn:(id)sender {
}

- (IBAction)pressLoginBtn:(id)sender {
    //get auth
    [XXTToolKit setName:self.accountTextField.text];
    [XXTToolKit setPasswd:self.passwdTextField.text];

    [self requestToLogin];
}

- (IBAction)accountTextFieldEndEditing:(id)sender {
    [self.passwdTextField becomeFirstResponder];
}

- (IBAction)passwdTextFieldEndEditing:(id)sender {
    [self pressLoginBtn:nil];
}

#pragma mark - URLRequest
- (void)requestToLogin{
    [SVProgressHUD showWithStatus:@"正在登陆"];
    
    NSString *url = [NSString stringWithFormat:@"%@?op=login&name=%@&passwd=%@", BASE_URL, self.accountTextField.text, self.passwdTextField.text];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(NSDictionary *responObject) {
        NSDictionary *dataDict = [responObject objectForKey:kRequestData];
        NSDictionary *userInfoDict = @{
                                       kInfoDictName: emptyStr([dataDict objectForKey:kRequestName]),
                                       kInfoDictNick: emptyStr([dataDict objectForKey:kRequestNick]),
                                       kInfoDictDegree: emptyStr([dataDict objectForKey:kRequestDegree]),
                                       kInfoDictAvatarURL: emptyStr([dataDict objectForKey:kRequestAvatarURL]),
                                       };
        [XXTToolKit saveUserData:userInfoDict];
        [XXTToolKit setMAuth:emptyStr([dataDict objectForKey:kRequestMAuth])];
        
        [SVProgressHUD showSuccessWithStatus:@"登陆成功"];
        [XXTToolKit setHasLogined:YES];
        
        if ([[XXTToolKit getAppdelegate] isReLogin]) {
            [[XXTToolKit getAppdelegate] setIsReLogin:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFI_REFRESH_ALL_CON object:nil];
        } else {
            [[[XXTToolKit getAppdelegate] navigationController] pushViewController:[[XXTToolKit getAppdelegate] getMainTabBarController] animated:NO];
        }

        DISMISS_VIEWCONTROLLER(self.navigationController, NO);
    } errorBlock:^(NSString *str){
        [SVProgressHUD showErrorWithStatusNoAutoDismiss:@"登陆出错, 检查你的密码或者用户名"];
        [self performBlock:^(id sender) {
            [SVProgressHUD dismiss];
        } afterDelay:2.0f];
        
    } failBlock:^{
        
    }];
}

@end


























