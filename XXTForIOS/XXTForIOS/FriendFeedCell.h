//
//  FriendFeedCell.h
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowLayoutLabel.h"

@interface FriendFeedCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet FlowLayoutLabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *titleBgImageView;
@property (strong, nonatomic) IBOutlet FlowLayoutLabel *hitLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hitBgImageView;

+ (CGFloat)hegihtFromTitle:(NSString *)title;
- (void)setDataWithAvatarURLStr:(NSString *)avatarURLStr
                           name:(NSString *)name
                    dateTimeStr:(NSString *)dateTimeStr
                          title:(NSString *)title
                            hit:(int)hit;

@end
