//
//  AboutUsViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-3.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTViewController.h"

@interface AboutUsViewController : XXTViewController
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;

@end
