//
//  CalendarCell.h
//  XXTForIOS
//
//  Created by pandara on 13-11-29.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *theTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *theTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *remindLabel;

- (void)setDataWithTitle:(NSString *)title
              createTime:(NSString *)createTime
              remindTime:(NSString *)remindTime
               isExpired:(BOOL)isExpired;

@end
