//
//  XXTTableView.m
//  XXTForIOS
//
//  Created by pandara on 13-9-19.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTTableView.h"

@interface XXTTableView ()



@end

@implementation XXTTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _slimeView = [[SRRefreshView alloc] init];
//        _slimeView.upInset = 44;
        _slimeView.slimeMissWhenGoingBack = YES;
        _slimeView.slime.bodyColor = [UIColor blackColor];
        _slimeView.slime.skinColor = [UIColor whiteColor];
        _slimeView.slime.lineWith = 1;
        _slimeView.slime.shadowBlur = 4;
        _slimeView.slime.shadowColor = [UIColor blackColor];
        
        [self addSubview:_slimeView];
    }
    return self;
}

- (void)setDelegate:(id<UITableViewDelegate>)delegate
{
    [super setDelegate:delegate];
    if ([delegate respondsToSelector:@selector(slimeRefreshStartRefresh:)]) {
        _slimeView.delegate = delegate;
    }
}

@end
