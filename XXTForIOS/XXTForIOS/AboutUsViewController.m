//
//  AboutUsViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-3.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"bg_texture.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    self.appNameLabel.alpha = 0;
    [XXTToolKit setView:self.appNameLabel toOrigin:CGPointMake(123, 289 - 44)];
    [UIView animateWithDuration:3.0f animations:^{
        self.appNameLabel.alpha = 1;
    } completion:^(BOOL finished) {
       [UIView animateWithDuration:3.0f animations:^{
           [XXTToolKit setView:self.appNameLabel toOrigin:CGPointMake(123, 269 - 44)];
       }];
    }];
    
    self.navigationItem.title = @"关于我们";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAppNameLabel:nil];
    [super viewDidUnload];
}
@end
