//
//  SBTabBarController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SBTabBarController.h"
#import "TabBarFooter.h"

@interface SBTabBarController ()

@end

@implementation SBTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationController.navigationBarHidden = NO;
    [super viewDidLoad];
    _currnetIndex = 1;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //自定义底部栏
    TabBarFooter *footer = [[TabBarFooter alloc] init];
    CGRect frame = footer.frame;
    frame.size = CGSizeMake(DEVICE_SIZE.width, 49);
    frame.origin = CGPointMake(0, self.view.frame.size.height - frame.size.height);
    footer.frame = frame;
    [footer setHandleBlock:^(int buttonIndex) {
        self.selectedIndex = buttonIndex;
    }];
    [footer setSelectedIndex:_currnetIndex];
    [self.view addSubview:footer];
    
    if (_localNotificaiton) {
        [[XXTToolKit getAppdelegate] handleLocalNotificaiton:_localNotificaiton];
        _localNotificaiton = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
    _currnetIndex = selectedIndex;
    if (selectedIndex != 2 && [APP_NAVIGATIONBAR.items count] != 0) {
        [APP_NAVIGATIONBAR_FIRSTITEM setRightBarButtonItem:nil];
    }
    
    UILabel *atLabel = (UILabel *)[self.navigationController.navigationBar viewWithTag:kViewTagATLabel];
    
    if (atLabel == nil) {
        return;
    }
    
    if (selectedIndex != 0) {//隐藏@ label
        atLabel.hidden = YES;
    } else {
        atLabel.hidden = NO;
    }
}

@end
