//
//  LoadingView.h
//  XXTForIOS
//
//  Created by pandara on 13-9-15.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LoadingView : UIView

@property (strong, nonatomic) IBOutlet UILabel *atLabel;
@property (strong, nonatomic) CABasicAnimation *rotationAnimation;

- (void)beginAnimation;
- (void)stopAnimation;
@end
