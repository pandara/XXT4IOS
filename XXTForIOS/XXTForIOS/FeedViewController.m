//
//  FeedViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FeedViewController.h"
#import "FriendFeedViewController.h"
#import "PostFeedViewController.h"
#import "CalendarViewController.h"
#import "MyFeedViewController.h"
#import "UIImageView+JMImageCache.h"
#import <QuartzCore/QuartzCore.h>
#import "DetailInfoViewController.h"
#import "HomePageViewController.h"

@interface FeedViewController ()

@end

@implementation FeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contactButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.friendFeedButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.scrollView.contentSize = CGSizeMake(DEVICE_SIZE.width, self.menuView.frame.origin.y + self.menuView.frame.size.height);

    self.avatarImageView.layer.borderColor = color(100, 100, 100, 1).CGColor;
    self.avatarImageView.layer.borderWidth = 1.0f;
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImageView.clipsToBounds = YES;
//    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//    UIImage *normalImage = [XXTToolKit getImageFromColor:color(17, 119, 204, 1) withRect:rect];
//    UIImage *highLightImage = [XXTToolKit getImageFromColor:color(20, 149, 255, 1) withRect:rect];
    
}

- (void)viewWillAppear:(BOOL)animated//will apear才能设置navigationItem，因为点击下方的button的时候不会再跑viewDidLoad
{
    [super viewWillAppear:animated];
    
    //设置导航栏
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more_button.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(pressMoreButton:)];
    
    [APP_NAVIGATIONBAR_FIRSTITEM setRightBarButtonItem:rightItem];
    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"动态"];
    
    [self.avatarImageView setImageWithURL:[NSURL URLWithString:[XXTToolKit getAvatarURLStr]] placeholder:[UIImage imageNamed:@"avatar_default_rect.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressMoreButton:(id)sender
{
    PostFeedViewController *con = [[PostFeedViewController alloc] initWithNibName:@"PostFeedViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

- (void)viewDidUnload {
    [self setFriendFeedButton:nil];
    [self setFriendFeedButton:nil];
    [self setContactButton:nil];
    [self setScrollView:nil];
    [self setMenuView:nil];
    [self setBgImageView:nil];
    [self setAvatarImageView:nil];
    [super viewDidUnload];
}

- (IBAction)pressFriendFeedButton:(id)sender {
    FriendFeedViewController *con = [[FriendFeedViewController alloc] initWithNibName:@"FriendFeedViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}
- (IBAction)pressCalendarButton:(id)sender {
    CalendarViewController *con = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}
- (IBAction)pressMyFeedButton:(id)sender {
    MyFeedViewController *con = [[MyFeedViewController alloc] initWithNibName:@"FriendFeedViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

- (IBAction)pressAddressBookButton:(id)sender {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    PRESENT_VIEWCONTROLLER(picker, self, YES);
}

- (IBAction)pressDetailInfoButton:(id)sender {
    DetailInfoViewController *infoViewController = [[DetailInfoViewController alloc] initWithNibName:@"DetailInfoViewController" bundle:nil targetName:[XXTToolKit getName]];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

- (IBAction)pressHomePageBtn:(id)sender {
    HomePageViewController *homePageViewCon = [[HomePageViewController alloc] initWithNibName:@"HomePageViewController" bundle:nil];
    [self.navigationController pushViewController:homePageViewCon animated:YES];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    DISMISS_VIEWCONTROLLER(peoplePicker, YES);
}



//#pragma mark - UIScrollViewDelegate
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGFloat interval = self.menuView.frame.origin.y - scrollView.contentOffset.y;
//    if (interval >= 0 && interval <= self.bgImageView.frame.size.height) {
//        CGPoint bgImageViewCenter = self.bgImageView.center;
//        bgImageViewCenter.y = self.scrollView.contentOffset.y + interval/2;
//        self.bgImageView.center = bgImageViewCenter;
//    }
//}

@end
