//
//  XXTDatePicker.m
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTDatePicker.h"

@implementation XXTDatePicker

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"XXTDatePicker" owner:self options:nil] objectAtIndex:0];
    if (self) {
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"XXTDatePicker" owner:self options:nil] objectAtIndex:0];;
    if (self) {
        self.frame = frame;
    }
    return self;
}

- (void)setOKBlock:(VoidBlock)block
{
    _okBlock = block;
}

- (void)setDateChangeBlock:(DateBlock)block
{
    _dateChangeBlock = block;
}

- (IBAction)pressOKButton:(id)sender {
    if (_okBlock) {
        _okBlock();
    }
}

- (IBAction)pickerValueChange:(id)sender {
    UIDatePicker *picker = (UIDatePicker *)sender;
    if (_dateChangeBlock) {
        _dateChangeBlock(picker.date);
    }
}

- (void)setDate:(NSDate *)date
{
    [self.datePicker setDate:date animated:NO];
}

- (void)setDate:(NSDate *)date animated:(BOOL)animated
{
    [self.datePicker setDate:date animated:animated];
}

- (void)setMiniDate:(NSDate *)miniDate
{
    self.datePicker.minimumDate = miniDate;
}

@end
