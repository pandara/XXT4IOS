//
//  ScholatViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXTTableView.h"
#import "XXTViewController.h"

@interface ScholatViewController : XXTViewController <XXTTableViewDelegate, XXTTableViewDataSource, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UILabel *atLabel;
@property (strong, nonatomic) NSMutableArray *chatArray;
@property (strong, nonatomic) NSMutableArray *searchResultArray;

@end
