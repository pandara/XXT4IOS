//
//  BubbleLoadMoreHead.h
//  familyhd
//
//  Created by pandara on 13-12-5.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BubbleLoadMoreHead;
@protocol BubbleLoadMoreHeadDelegate <NSObject>

- (void)loadMoreHeadDidStartToLoad:(BubbleLoadMoreHead *)head;

@end

@interface BubbleLoadMoreHead : UIView {
    BOOL _isLoading;
    
    CGFloat _originalContentSizeH;//用于在endLoadMore中判断是否需要设置edgeinset
}
@property (strong, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *juhua;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) id <BubbleLoadMoreHeadDelegate> delegate;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)endLoadMore;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)scrollViewDidEndDecelerate:(UIScrollView *)scrollView;
- (void)setToLoadMore;

@end
