//
//  PostCalendarViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "PostCalendarViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "XXTDatePicker.h"
#import "XXTHttpClient.h"
#import "BlocksKit.h"


@interface PostCalendarViewController () {
    NSDate *_alarmDate;
}

@property (strong, nonatomic) XXTDatePicker *datePicker;

@end

@implementation PostCalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = BASEBACKGROUNDCOLOR;
    
    //导航栏
    self.navigationItem.title = @"设置日程";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleBordered target:self action:@selector(pressSaveButton:)];
    self.navigationItem.rightBarButtonItem = rightItem;

    //设置控件
    self.textView.layer.borderColor = color(0, 0, 0, 1).CGColor;
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.cornerRadius = 5.0f;
    self.textView.placeholder = @"写下您的日程内容(向下滑动收起键盘)";
    
    self.alarmButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.alarmButton.layer.borderWidth = 1.0f;
    self.alarmButton.layer.cornerRadius = 5.0f;
    [self.alarmButton addTarget:self action:@selector(pressAlarmButton:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateStr = [dateFormatter stringFromDate:today];
    _alarmDate = today;
    [self.alarmButton setTitle:dateStr forState:UIControlStateNormal];
    
}


- (void)pressSaveButton:(id)sender
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFI_NEW_CALENDAR object:nil userInfo:@{
                                         kRequestAlarmDateLine: [NSString stringWithFormat:@"%f", [_alarmDate timeIntervalSince1970]],
                                               kRequestContent: self.textView.text,
                                        kRequestCreateDateLine: [NSString stringWithFormat:@"%f", [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]],
                                                kRequestOption: @"0"
     }];
    
    [self dismissTheDatePicker];
    [self moveTheMainViewDown];
    
    [KGStatusBar showWithStatus:@"正在向云端同步日历"];
    [self performBlock:^(id sender) {
        [KGStatusBar dismiss];
        [self performBlock:^(id sender) {
            [self requestToPostCalendar:self.textView.text alarmDate:_alarmDate completeBlock:^{
                [KGStatusBar showSuccessWithStatus:@"同步成功"];
            } errorBlock:^(NSString *str) {
                [KGStatusBar showErrorWithStatus:str];
            }];
        } afterDelay:0.5f];
    } afterDelay:1.0f];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pressAlarmButton:(UIButton *)button
{
    if (!_datePicker) {
        [self addTheDatePicker];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *presetDateStr = self.alarmButton.titleLabel.text;
    NSDate *presetDate = [formatter dateFromString:presetDateStr];
    _datePicker.date = presetDate;
    _datePicker.miniDate = presetDate;
    
    [self moveTheMainViewUP];
    [self showTheDatePicker];
}

- (void)addTheDatePicker
{
    _datePicker = [[XXTDatePicker alloc] init];
    
    //按下 完成 按钮
    VoidBlock okBlock = ^(){
        [self dismissTheDatePicker];
        [self moveTheMainViewDown];
    };
    [_datePicker setOKBlock:okBlock];
    
    //滑动 日期选择器
    DateBlock dateChangeBlock = ^(NSDate *date) {
        _alarmDate = date;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *dateStr = [formatter stringFromDate:date];
        [self.alarmButton setTitle:dateStr forState:UIControlStateNormal];
    };
    [_datePicker setDateChangeBlock:dateChangeBlock];
    
    //add subview
    [XXTToolKit setView:_datePicker toOrigin:CGPointMake(0, DEVICE_SIZE.height)];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:_datePicker];
}

- (void)moveTheMainViewUP
{
    CGFloat interval = self.alarmButton.frame.size.height + self.alarmButton.frame.origin.y - (WINDOW_SIZE.height - _datePicker.frame.size.height);
    if (interval > 0) {
        [UIView animateWithDuration:0.3f animations:^{
            [XXTToolKit setView:self.view toOriginY:-interval - 10 - 44 - 20];
        }];
    }
}

- (void)moveTheMainViewDown
{
    [UIView animateWithDuration:0.3f animations:^{
        [XXTToolKit setView:self.view toOriginY:0];
    }];
}

- (void)showTheDatePicker
{
    [UIView animateWithDuration:0.3f animations:^{
        [XXTToolKit setView:_datePicker toOrigin:CGPointMake(0, DEVICE_SIZE.height - _datePicker.frame.size.height + 20)];
    }];
}

- (void)dismissTheDatePicker
{
    [UIView animateWithDuration:0.3f animations:^{
        [XXTToolKit setView:_datePicker toOrigin:CGPointMake(0, DEVICE_SIZE.height + 20)];
    }];
}

- (void)removeTheDatePicker
{
    if (_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self removeTheDatePicker];
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload {
    [self removeTheDatePicker];
    [self setAlarmButton:nil];
    [super viewDidUnload];
}

#pragma mark - URLRequest
- (void)requestToPostCalendar:(NSString *)content alarmDate:(NSDate *)alarmDate completeBlock:(VoidBlock)completeBlock errorBlock:(XXTHttpErrorBlock)errorBlock
{
    
    NSString *url = [NSString stringWithFormat:@"%@?op=postcalendar", BASE_URL];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDictionary *para = @{
                           kRequestMAuth: [XXTToolKit getMAuth],
                           kRequestName: [XXTToolKit getName],
                           kRequestAlarmDay: [dateFormatter stringFromDate:alarmDate],
                           kRequestContent: content,
                           };
    
    XXTLog(@"paras:%@", para);
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:para successBlock:^(NSDictionary *responObject) {
        if (completeBlock) {
            completeBlock();
        }
    } errorBlock:^(NSString *str) {
        if (errorBlock) {
            errorBlock(str);
        }
    } failBlock:^{
        
    }];
}

@end













