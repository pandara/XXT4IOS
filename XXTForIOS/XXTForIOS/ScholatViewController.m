//
//  ScholatViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "ScholatViewController.h"
#import "AppDelegate.h"
#import "XXTTableCell.h"
#import "UIButton+Block.h"
#import "BlocksKit.h"
#import "XXTHttpClient.h"
#import <QuartzCore/QuartzCore.h>
#import "ChatingDetailViewController.h"

@interface ScholatViewController ()

@property (strong, nonatomic) UIButton *searchMaskButton;
@property (strong, nonatomic) NSArray *testTitleArray;
@property (strong, nonatomic) NSArray *testSubTitleArray;
@property (strong, nonatomic) NSArray *testTimeArray;

@end

@implementation ScholatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //chatArray
    self.chatArray = [[NSMutableArray alloc] init];
    [self.chatArray addObjectsFromArray:[XXTToolKit getDataFromAllChatListCache]];
    
    self.searchResultArray = [[NSMutableArray alloc] init];
    [self.searchResultArray addObjectsFromArray:self.chatArray];
    
    //searchMaskButton;
    _searchMaskButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    _searchMaskButton.backgroundColor = color(0, 0, 0, 0.5);
    [_searchMaskButton handleControlEvent:UIControlEventTouchUpInside withBlock:^{
        [self.searchBar resignFirstResponder];
    }];
    
    //设置searchBar
//    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, 44)];
    self.searchBar.frame = CGRectMake(0, 0, DEVICE_SIZE.width, 44);
    self.searchBar.placeholder = @"搜索";
    self.searchBar.delegate = self;
    
    UIView *searchBarBgView = [[self.searchBar subviews] objectAtIndex:0];
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, searchBarBgView.frame.size.width, searchBarBgView.frame.size.height)];
    bgImageView.image = [UIImage imageNamed:@"searchbar_bg.png"];
    [searchBarBgView addSubview:bgImageView];
    
    //tableView
//    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height - NAVIGATIONBAR_HEIGHT - TABBAR_HEIGHT)];
    self.tableView.frame = CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height - NAVIGATIONBAR_HEIGHT - TABBAR_HEIGHT);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = self.searchBar;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"SCHOL@"];
    
    [self requestChatList];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self stopRotatingATLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)startRotatingATLabel
//{
//    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"SCHOL    "];
//    //设置atLabel
//    self.atLabel = [[UILabel alloc] initWithFrame:CGRectMake(185, 9 + 20, 25, 25)];
//    self.atLabel.tag = kViewTagATLabel;
//    self.atLabel.text = @"@";
//    self.atLabel.textColor = [UIColor whiteColor];
//    self.atLabel.backgroundColor = [UIColor clearColor];
//    self.atLabel.font = [UIFont systemFontOfSize:21];
//    [self.navigationController.view addSubview:self.atLabel];
//    
//    //增加@的动画
//    CABasicAnimation* rotationAnimation;
//    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
//    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 4];
//    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    rotationAnimation.duration = 1.2f;
//    rotationAnimation.repeatCount = 100;//动画执行两次
//    [self.atLabel.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
//}
//
//- (void)stopRotatingATLabel
//{
//    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"SCHOL@"];
//    if (self.atLabel) {
//        [self.atLabel removeFromSuperview];
//        self.atLabel = nil;
//    }
//}

- (void)viewDidUnload {
    [self setSearchBar:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}

- (void)refreshSearchResult
{
    NSString *searchName = self.searchBar.text;
    [self.searchResultArray removeAllObjects];
    
    if (searchName.length == 0) {
        [self.searchResultArray addObjectsFromArray:self.chatArray];
        [self.tableView reloadData];
        return;
    }
    
    for (NSDictionary *chatInfo in self.chatArray) {
        NSDictionary *toUserInfo = [chatInfo objectForKey:kRequestToUserInfo];
        NSString *showName = [XXTToolKit getNameFromNick:emptyStr([toUserInfo objectForKey:kRequestNick]) andName:emptyStr([toUserInfo objectForKey:kRequestName])];
        
        if ([showName rangeOfString:searchName].location != NSNotFound) {
            [self.searchResultArray addObject:chatInfo];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResultArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XXTTableCell *cell = [tableView dequeueReusableCellWithIdentifier:XXTTABLECELL_ID];
    if (cell == nil) {
        cell = [[XXTTableCell alloc] init];
        [cell setStyle:XXTTableCellStyleWithTime];
    }
    
    NSDictionary *chatDict = [self.searchResultArray objectAtIndex:indexPath.row];
    NSDictionary *toUserDict = [chatDict objectForKey:kRequestToUserInfo];
    
    [cell setImageURLStr:emptyStr([toUserDict objectForKey:kRequestAvatarURL])
                   title:[XXTToolKit getNameFromNick:emptyStr([toUserDict objectForKey:kRequestNick])
                                             andName:emptyStr([toUserDict objectForKey:kRequestName])]
                subTitle:emptyStr([chatDict objectForKey:kRequestContent])
                 dateStr:[XXTToolKit getFormatDateStrFromDateLine:emptyStr([chatDict objectForKey:kRequestDateLine])]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *chatInfoDict = [self.searchResultArray objectAtIndex:indexPath.row];
    NSDictionary *toUserDict = [chatInfoDict objectForKey:kRequestToUserInfo];
    NSString *toUserName = [toUserDict objectForKey:kRequestName];
    NSString *toUserNick = [toUserDict objectForKey:kRequestNick];
    
    ChatingDetailViewController *con = [[ChatingDetailViewController alloc] initWithNibName:@"ChatingDetailViewController" bundle:nil withToUserName:toUserName andToUserNick:toUserNick];
    [self.navigationController pushViewController:con animated:YES];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (![_searchMaskButton superview]) {
        _searchMaskButton.alpha = 0;
        [self.view addSubview:_searchMaskButton];
        
        [UIView animateWithDuration:0.2f animations:^{
            _searchMaskButton.alpha = 1;
        }];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refreshSearchResult];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([_searchMaskButton superview]) {
        [UIView animateWithDuration:0.2f animations:^{
            _searchMaskButton.alpha = 0;
        } completion:^(BOOL finished) {
            [_searchMaskButton removeFromSuperview];
        }];
    }
}

//#pragma mark - ScrollViewDelegate
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self.tableView.slimeView scrollViewDidScroll];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    [self.tableView.slimeView scrollViewDidEndDraging];
//}
//
//#pragma mark - SRRefreshDelegate
//- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView
//{
//    [self performBlock:^(id sender) {
//        [self.tableView.slimeView endRefresh];
//    } afterDelay:2.0f];
//}

#pragma mark - URLRequest
- (void)requestChatList {
    NSString *url = [NSString stringWithFormat:@"%@?op=allchatlist&mauth=%@&name=%@&page=0&perpage=1000", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName]];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(NSDictionary *responObject) {
        [self.chatArray removeAllObjects];
        [self.chatArray addObjectsFromArray:[responObject objectForKey:kRequestData]];
        [self refreshSearchResult];
        [XXTToolKit setDataToAllChatListCache:self.chatArray];
        [self.tableView reloadData];
    } failBlock:^{
        
    }];
}

@end
