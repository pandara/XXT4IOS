//
//  CalendarFireViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-12-2.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "CalendarFireViewController.h"
#import "MetalBgView.h"

@interface CalendarFireViewController () {
    BOOL _shouldStopHandAnimateContinue;
}

@end

@implementation CalendarFireViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_notification) {
        MetalBgView *bgView = [[MetalBgView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height)];
        [self.view insertSubview:bgView atIndex:0];
        
        NSDictionary *dataDict = _notification.userInfo;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM-dd HH:mm"];
        _timeLabel.text = [formatter stringFromDate:[dataDict objectForKey:kNotifiUserInfoAlarmDate]];
        
        _contentLabel.text = [dataDict objectForKey:kNotifiUserInfoContent];
        
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 4];
        rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        rotationAnimation.duration = 2.0f;
        rotationAnimation.repeatCount = 100;
        [_stopButton.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        
        [self handAnimation];
    }
    
    
}

- (void)handAnimation
{
    if (_shouldStopHandAnimateContinue) {
        return;
    }
    
    CGFloat interval = 30;
    CGFloat dur = 1.0f;
    
    [UIView animateWithDuration:dur animations:^{
        [XXTToolKit setView:_handImageView toOriginY:_handImageView.frame.origin.y + interval];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:dur animations:^{
            [XXTToolKit setView:_handImageView toOriginY:_handImageView.frame.origin.y - interval];
        } completion:^(BOOL finished) {
            [self handAnimation];
        }];
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_stopButton.layer removeAllAnimations];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setHandImageView:nil];
    [self setStopButton:nil];
    [self setContentLabel:nil];
    [self setTimeLabel:nil];
    [super viewDidUnload];
}

- (IBAction)pressStopButton:(id)sender {
    NSDictionary *userInfo = _notification.userInfo;
    NSString *currentID = [userInfo objectForKey:kNotifiUserInfoID];
    
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *notificaiton in notificationArray) {
        NSString *calendarID = [notificaiton.userInfo objectForKey:kNotifiUserInfoID];
        if ([calendarID isEqualToString:currentID]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notificaiton];
            break;
        }
    }
    
    DISMISS_VIEWCONTROLLER(self, YES);
}

@end
