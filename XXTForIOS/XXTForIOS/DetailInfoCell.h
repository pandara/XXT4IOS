//
//  DetailInfoCell.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

- (void)setDataWithContentText:(NSString *)contentText;

@end
