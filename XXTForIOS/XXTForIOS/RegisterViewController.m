//
//  RegisterViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-8.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "RegisterViewController.h"
#import "XXTHttpClient.h"
#import "SVProgressHUD.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIImage *registbtnImage;
    UIImage *registBtnImageHL;
    
    const CGFloat resizeImageInset = 10;
    const CGFloat resizeImageInsetLeft = 10;
    
    if (DEVICE_OS_VERSION < 6.0f) {
        registbtnImage = [[UIImage imageNamed:@"btn_green.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
        registBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
    } else if (DEVICE_OS_VERSION >= 6.0f) {
        registbtnImage = [[UIImage imageNamed:@"btn_green.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
        registBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
    }
    
    [self.registButton setBackgroundImage:registbtnImage forState:UIControlStateNormal];
    [self.registButton setBackgroundImage:registBtnImageHL forState:UIControlStateHighlighted];
    
    self.navigationItem.title = @"注册";
    self.navigationItem.backBarButtonItem.title = @"返回";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setNameTextField:nil];
    [self setPasswdTextField:nil];
    [self setNickTextField:nil];
    [self setRegistButton:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.5f animations:^{
        self.navigationController.navigationBarHidden = NO;
    }];
}

- (IBAction)pressRegistButton:(id)sender {
    if (self.nameTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"用户名不能为空"];
        return;
    }
    
    if (self.passwdTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"密码不能为空"];
    }
    
    [SVProgressHUD showWithStatus:@"正在注册"];
    [self requestToRegist];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nameTextField resignFirstResponder];
    [self.passwdTextField resignFirstResponder];
    [self.nickTextField resignFirstResponder];
}

#pragma mark - URLRequest
- (void)requestToRegist
{
    NSString *url = [NSString stringWithFormat:@"%@?op=regist", BASE_URL];
    NSDictionary *para = @{
        kRequestName: self.nameTextField.text,
        kRequestPasswd: self.passwdTextField.text,
        kRequestNick: self.nickTextField.text,
    };
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:para successBlock:^(NSDictionary *responObject) {
        NSDictionary *dataDict = [responObject objectForKey:kRequestData];
        NSDictionary *userInfoDict = @{
            kInfoDictName: emptyStr([dataDict objectForKey:kRequestName]),
            kInfoDictNick: emptyStr([dataDict objectForKey:kRequestNick]),
            kInfoDictDegree: emptyStr([dataDict objectForKey:kRequestDegree]),
            kInfoDictAvatarURL: emptyStr([dataDict objectForKey:kRequestAvatarURL]),
        };
        [XXTToolKit saveUserData:userInfoDict];
        [XXTToolKit setMAuth:emptyStr([dataDict objectForKey:kRequestMAuth])];
        
        [SVProgressHUD showSuccessWithStatus:@"注册成功"];
        [XXTToolKit setHasLogined:YES];
        
        if ([[XXTToolKit getAppdelegate] isReLogin]) {
            [[XXTToolKit getAppdelegate] setIsReLogin:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFI_REFRESH_ALL_CON object:nil];
        } else {
            [[[XXTToolKit getAppdelegate] navigationController] pushViewController:[[XXTToolKit getAppdelegate] getMainTabBarController] animated:NO];
        }
        
        DISMISS_VIEWCONTROLLER(self.navigationController, NO);
    } errorBlock:^(NSString *str) {
        [SVProgressHUD showErrorWithStatus:str];
    } failBlock:^{
        
    }];
}

@end













