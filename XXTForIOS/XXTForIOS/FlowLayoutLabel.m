//
//  FlowLayoutLabel.m
//  family_ver_pm
//
//  Created by pandara on 13-4-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FlowLayoutLabel.h"

@implementation FlowLayoutLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.lineBreakMode = NSLineBreakByWordWrapping | NSLineBreakByClipping;
    }
    return self;
}

- (void)setMaxWidth:(int)smaxWidth
            maxLine:(int)smaxLine
               font:(UIFont *)sfont
           fontSize:(CGFloat)sfontSize
{
    fontSize = sfontSize;
    maxWidth = smaxWidth;
    self.font = sfont;
    self.numberOfLines = smaxLine;
    numberOfLines = smaxLine;
}

//- (void)setMaxWidth:(int)smaxWidth maxLine:(int)smaxLine font:(UIFont *)sfont
//{
//    [self setMaxWidth:smaxWidth maxLine:smaxLine font:sfont fontSize:TITLE_FONT_SIZE];
//}

+ (CGSize)getHeightFromText:(NSString *)text maxWidth:(int)maxWidth maxLine:(int)maxLine font:(UIFont *)font fontSize:(CGFloat)fontSize
{
    FlowLayoutLabel *label = [[FlowLayoutLabel alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 10)];
    [label setMaxWidth:maxWidth maxLine:maxLine font:font fontSize:fontSize];
    [label setTextContent:text];
    return label.frame.size;
}

- (CGSize)setTextContent:(NSString *)text
{
    //ios6
    CGFloat osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    self.lineBreakMode = NSLineBreakByCharWrapping;
    self.text = text;
    
    if (osVersion >= 6) {
        CGRect originFrame = self.frame;
        self.frame = CGRectMake(originFrame.origin.x, originFrame.origin.y, maxWidth, 200);
        [self sizeToFit];
        CGRect frame = self.frame;
        frame.size.width = MIN(maxWidth, frame.size.width);
        self.frame = frame;
    } else {
        if (numberOfLines != 0) {
            CGSize flowSize = [text sizeWithFont:self.font constrainedToSize:CGSizeMake(maxWidth, 1000.0f) lineBreakMode:NSLineBreakByWordWrapping | NSLineBreakByClipping ];
            CGSize sampleSize;
            
            //取自适应label的高度
            NSString *sampleString = @"";
            for (int i = 0; i < numberOfLines; i++) {
                sampleString = [NSString stringWithFormat:@"%@a", sampleString];
            }
            
            sampleSize = [sampleString sizeWithFont:self.font constrainedToSize:CGSizeMake(fontSize, 1000.0f) lineBreakMode:NSLineBreakByWordWrapping | NSLineBreakByClipping];
            
//            numberOfLines = 0;
            //高度取实际高度与最大高度的较小者
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, flowSize.width, sampleSize.height < flowSize.height? sampleSize.height:flowSize.height);
        } else {
//            CGSize flowSize = [text sizeWithFont:self.font constrainedToSize:CGSizeMake(maxWidth, 1000.0f) lineBreakMode:NSLineBreakByWordWrapping];
            CGRect frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, maxWidth, 10);
            self.frame = frame;
            [self sizeToFit];
        }
    }
    
    return self.frame.size;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
