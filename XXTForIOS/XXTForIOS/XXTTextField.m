//
//  XXTTextField.m
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTTextField.h"

@implementation XXTTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        self.background = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg.png"]
                                           withInsetTop:10
                                                   left:10
                                                 bottom:10
                                                  right:10];
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, frame.size.height)];
        leftView.userInteractionEnabled = NO;
        self.leftView = leftView;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        _innerDelegate = [[XXTTextFieldInnerDelegate alloc] init];
        self.delegate = _innerDelegate;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}

- (void)setDelegateInXXTTextField:(id<XXTTextFieldDelegate>)delegateInXXTTextField
{
    _delegateInXXTTextField = delegateInXXTTextField;
    _innerDelegate.delegateInXXTTextField = _delegateInXXTTextField;
}

@end
