//
//  XXTDatePicker.h
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XXTDatePicker : UIView {
    VoidBlock _okBlock;
    DateBlock _dateChangeBlock;
}

@property (strong, nonatomic) NSDate *date;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *miniDate;

- (void)setOKBlock:(VoidBlock)block;
- (void)setDateChangeBlock:(DateBlock)block;
- (IBAction)pickerValueChange:(id)sender;
- (void)setDate:(NSDate *)date animated:(BOOL)animated;

@end
