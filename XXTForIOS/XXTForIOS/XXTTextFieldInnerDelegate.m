//
//  XXTTextFieldInnerDelegate.m
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTTextFieldInnerDelegate.h"

@implementation XXTTextFieldInnerDelegate

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_delegateInXXTTextField respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [_delegateInXXTTextField textFieldDidBeginEditing:(XXTTextField *)textField];
    }

    textField.background = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg_a.png"]
                                       withInsetTop:10
                                               left:10
                                             bottom:10
                                              right:10];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([_delegateInXXTTextField respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [_delegateInXXTTextField textFieldDidEndEditing:(XXTTextField *)textField];
    }
    
    textField.background = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg.png"]
                                       withInsetTop:10
                                               left:10
                                             bottom:10
                                              right:10];
}

@end
