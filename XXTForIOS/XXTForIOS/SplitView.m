//
//  SplitView.m
//  XXTForIOS
//
//  Created by pandara on 13-9-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SplitView.h"

@implementation SplitView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self = [[[NSBundle mainBundle] loadNibNamed:@"SplitView" owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void)beginAnimation
{
    [UIView animateWithDuration:1.5f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect leftImageFrame = self.leftImageView.frame;
        CGRect rightImageFrame = self.rightImageView.frame;
        
        leftImageFrame.origin.x = -320;
        rightImageFrame.origin.x = 320;
        
        self.leftImageView.frame = leftImageFrame;
        self.rightImageView.frame = rightImageFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
