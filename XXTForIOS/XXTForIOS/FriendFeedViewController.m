//
//  FriendFeedViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FriendFeedViewController.h"
#import "XXTHttpClient.h"
#import "FriendFeedCell.h"
#import "BlocksKit.h"

@interface FriendFeedViewController ()

@property (strong, nonatomic) NSArray *feedArray;
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation FriendFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = color(235, 235, 235, 1);
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height - 44) style:UITableViewStylePlain];
    _tableView.separatorColor = TABLESEPARATORCOLOR;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    
    if (self.isMyFeedView) {
        _feedArray = [XXTToolKit getMyFeedList];
    } else {
        _feedArray = [XXTToolKit getFriendFeedList];
    }
    
    self.navigationItem.title = @"好友动态";
    
    [self startLoading];
    [self requestToGetFriendFeedListWithCompleteBlock:^{
        [self doneLoading];
        [_tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}

- (void)reloadData
{
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITabelViewDelegate UITableDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_feedArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = [_feedArray objectAtIndex:indexPath.row];
    return [FriendFeedCell hegihtFromTitle:emptyStr([dataDict objectForKey:kRequestTitle])];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIDFriendFeedCell];
    if (cell == nil) {
        cell = [[FriendFeedCell alloc] init];
    }
    
    NSDictionary *dataDict = [_feedArray objectAtIndex:indexPath.row];
    
    NSString *dateLine = emptyStr([dataDict objectForKey:kRequestDateLine]);
    NSString *dateTimeStr = [XXTToolKit getFormatDateStrFromDateLine:dateLine];
    
    NSString *showName = [XXTToolKit getNameFromNick:[dataDict objectForKey:kRequestNick] andName:[dataDict objectForKey:kRequestName]];
    
    [cell setDataWithAvatarURLStr:emptyStr([dataDict objectForKey:kRequestAvatarURL])
                             name:showName
                      dateTimeStr:dateTimeStr
                            title:emptyStr([dataDict objectForKey:kRequestTitle])
                              hit:[emptyStr([dataDict objectForKey:kRequestHeat]) intValue]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - URLRequest
- (void)requestToGetFriendFeedListWithCompleteBlock:(VoidBlock)block
{
    NSString *kind;
    if (_isMyFeedView) {
        kind = @"2";
    } else {
        kind = @"1";
    }
    NSString *url = [NSString stringWithFormat:@"%@?op=feedlist&mauth=%@&name=%@&page=0&perpage=100&kind=%@", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName], kind];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(id responObject) {
        _feedArray = [responObject objectForKey:kRequestData];
        if (self.isMyFeedView) {
            [XXTToolKit setMyFeedList:_feedArray];
        } else {
            [XXTToolKit setFriendFeedList:_feedArray];
        }
        if (block) {
            block();
        }
    } failBlock:^{
        
    }];
}

@end











    