//
//  PostFeedViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "PostFeedViewController.h"
#import "BlocksKit.h"
#import "SVProgressHUD.h"
#import "XXTHttpClient.h"

#define CONTROL_MARGIN 10

@interface PostFeedViewController ()

@end

@implementation PostFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"发表动态";
    UIBarButtonItem *postButton = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleBordered target:self action:@selector(pressPostButton:)];
    self.navigationItem.rightBarButtonItem = postButton;
    
    self.view.backgroundColor = BASEBACKGROUNDCOLOR;
    
    _textField = [[XXTTextField alloc] initWithFrame:CGRectMake(CONTROL_MARGIN, CONTROL_MARGIN, DEVICE_SIZE.width - CONTROL_MARGIN * 2, 40)];
    _textField.text = @"学讯通动态消息";
    _textField.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:_textField];
    
    _buttonGroup = [[SingleSelectButtonGroup alloc] initWithFrame:CGRectMake(CONTROL_MARGIN, _textField.frame.size.height + _textField.frame.origin.y + CONTROL_MARGIN, DEVICE_SIZE.width - CONTROL_MARGIN * 2, 40)];
    [self.view addSubview:_buttonGroup];
    
    _textView = [[XXTTextView alloc] initWithFrame:CGRectMake(CONTROL_MARGIN, _buttonGroup.frame.origin.y + _buttonGroup.frame.size.height + CONTROL_MARGIN, DEVICE_SIZE.width - CONTROL_MARGIN * 2, 80)];
    _textView.font = [UIFont systemFontOfSize:14];
    _textView.placeHolder = @"写下内容吧";
    [self.view addSubview:_textView];
    [_textView becomeFirstResponder];
}

- (void)pressPostButton:(id)sender
{
    if (_textField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"填上标题吧~"];
        return;
    }
    
    NSString *permissionStr;//0公平 1好友 2私有
    switch (_buttonGroup.selectedIndex) {
        case 0:
        {
            permissionStr = @"0";
        }
            break;
        case 1:
        {
            permissionStr = @"1";
        }
            break;
        case 2:
        {
            permissionStr = @"2";
        }
            break;
        default:
            break;
    }
    
    [KGStatusBar showWithStatus:@"正在发送动态"];
    [self performBlock:^(id sender) {
        [KGStatusBar dismiss];
        [self performBlock:^(id sender) {
            [self requestToPostFeedWithTitle:_textField.text content:_textView.textView.text permissionStr:permissionStr completeBlock:^{
                [KGStatusBar showSuccessWithStatus:@"发送动态成功"];
            } errorBlock:^(NSString *str) {
                [KGStatusBar showErrorWithStatus:str];
            }];
        } afterDelay:0.5f];
    } afterDelay:1.0f];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_textField resignFirstResponder];
    [_textView resignFirstResponder];
}

#pragma mark - URLRequest
- (void)requestToPostFeedWithTitle:(NSString *)title content:(NSString *)content permissionStr:(NSString *)permissionStr completeBlock:(VoidBlock)completeBlock errorBlock:(XXTHttpErrorBlock)errorBlock
{
    NSString *url = [NSString stringWithFormat:@"%@?op=postfeed", BASE_URL];
    NSDictionary *para = @{
                           kRequestMAuth: [XXTToolKit getMAuth],
                           kRequestName: [XXTToolKit getName],
                           kRequestTitle: title,
                           kRequestContent: content,
                           kRequestOption: permissionStr,
                           };

    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:para successBlock:^(NSDictionary *responObject) {
        
        if (completeBlock) {
            completeBlock();
        }
    } errorBlock:^(NSString *str) {
        if (errorBlock) {
            errorBlock(str);
        }
    } failBlock:^{
        
    }];
}

@end










