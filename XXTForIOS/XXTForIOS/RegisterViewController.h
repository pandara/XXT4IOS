//
//  RegisterViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-8.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTViewController.h"

@interface RegisterViewController : XXTViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwdTextField;
@property (weak, nonatomic) IBOutlet UITextField *nickTextField;
@property (weak, nonatomic) IBOutlet UIButton *registButton;

@end
