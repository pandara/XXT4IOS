//
//  UIBubbleTableView.h
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <UIKit/UIKit.h>

#import "UIBubbleTableViewDataSource.h"
#import "UIBubbleTableViewCell.h"
#import "BubbleLoadMoreHead.h"

typedef enum _NSBubbleTypingType
{
    NSBubbleTypingTypeNobody = 0,
    NSBubbleTypingTypeMe = 1,
    NSBubbleTypingTypeSomebody = 2
} NSBubbleTypingType;

@class UIBubbleTableView;
@protocol UIBubbleTableViewDelegate <NSObject>//add by pandara

@optional
- (void)bubbleTableViewToLoadMore:(UIBubbleTableView *)bubbleTableView;

@end

@interface UIBubbleTableView : UITableView <UITableViewDelegate, UITableViewDataSource, BubbleLoadMoreHeadDelegate>

@property (nonatomic, strong) id<UIBubbleTableViewDelegate> bubbleDelegate;
@property (nonatomic, strong) id<UIBubbleTableViewDataSource> bubbleDataSource;
@property (nonatomic) NSTimeInterval snapInterval;
@property (nonatomic) NSBubbleTypingType typingBubble;
@property (nonatomic) BOOL showAvatars;
@property (strong, nonatomic) BubbleLoadMoreHead *head;

- (void)scrollBubbleViewToBottomAnimated:(BOOL)animated;
- (void)reloadData:(BOOL)isPullingData;

@end
