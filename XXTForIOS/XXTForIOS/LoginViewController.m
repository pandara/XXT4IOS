//
//  LoginViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginSubViewController.h"
#import "RegisterViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *loginbtnImage;
    UIImage *loginBtnImageHL;
    UIImage *registImage;
    UIImage *registImageHL;
    
    const CGFloat resizeImageInset = 10;
    const CGFloat resizeImageInsetLeft = 10;
    
    if (DEVICE_OS_VERSION < 6.0f) {
        loginbtnImage = [[UIImage imageNamed:@"btn_green.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
        loginBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
        registImage = [[UIImage imageNamed:@"btn_white.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
        registImageHL = [[UIImage imageNamed:@"btn_white_a.png"] stretchableImageWithLeftCapWidth:resizeImageInsetLeft topCapHeight:resizeImageInset];
    } else if (DEVICE_OS_VERSION >= 6.0f) {
        loginbtnImage = [[UIImage imageNamed:@"btn_green.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
        loginBtnImageHL = [[UIImage imageNamed:@"btn_green_a.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
        registImage = [[UIImage imageNamed:@"btn_white.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
        registImageHL = [[UIImage imageNamed:@"btn_white_a.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(resizeImageInset, resizeImageInsetLeft, resizeImageInset, resizeImageInsetLeft) resizingMode:UIImageResizingModeStretch];
    }
    
    [self.loginButton setBackgroundImage:loginbtnImage forState:UIControlStateNormal];
    [self.loginButton setBackgroundImage:loginBtnImageHL forState:UIControlStateHighlighted];
    [self.registerButton setBackgroundImage:registImage forState:UIControlStateNormal];
    [self.registerButton setBackgroundImage:registImageHL forState:UIControlStateHighlighted];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.5f animations:^{
        self.navigationController.navigationBarHidden = YES;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLoginButton:nil];
    [self setRegisterButton:nil];
    [super viewDidUnload];
}

- (IBAction)pressLoginButton:(id)sender {
    LoginSubViewController *loginSubViewController = [[LoginSubViewController alloc] initWithNibName:@"LoginSubViewController" bundle:nil];
    [self.navigationController pushViewController:loginSubViewController animated:YES];
}

- (IBAction)pressRegistButton:(id)sender {
    RegisterViewController *con = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

@end







