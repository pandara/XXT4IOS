//
//  PostCalendarViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTViewController.h"
#import "XXTSlideTextView.h"

@interface PostCalendarViewController : XXTViewController

@property (unsafe_unretained, nonatomic) IBOutlet XXTSlideTextView *textView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *alarmButton;

@end
