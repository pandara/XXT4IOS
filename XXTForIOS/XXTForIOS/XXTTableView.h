//
//  XXTTableView.h
//  XXTForIOS
//
//  Created by pandara on 13-9-19.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRRefreshView.h"

@protocol XXTTableViewDelegate <NSObject, UITableViewDelegate, SRRefreshDelegate>

@end

@protocol XXTTableViewDataSource <NSObject, UITableViewDataSource>

@end

@interface XXTTableView : UITableView

@property (strong, nonatomic) SRRefreshView *slimeView;

@end
