//
//  TapButton.h
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TabButtonBlock) (void);
typedef enum {
    TabButtonStateNormal,
    TabButtonStateHighLight,
}TabButtonState;

@interface TabButton : UIView {
    TabButtonBlock block;
}

@property (strong, nonatomic) UIImage *highLightImage;
@property (strong, nonatomic) UIImage *normalImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *highLightView;

- (void)setOrigin:(CGPoint)origin;
- (void)setState:(TabButtonState)state;
- (void)setHandlerBlockWhenTap:(TabButtonBlock)handlerBlock;
- (void)setImageInsetWithTop:(CGFloat)top andLeft:(CGFloat)left;
- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage withTitle:(NSString *)title;
@end
