//
//  PostFeedViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXTTextField.h"
#import "SingleSelectButtonGroup.h"
#import "XXTTextView.h"

@interface PostFeedViewController : UIViewController

@property (strong, nonatomic) SingleSelectButtonGroup *buttonGroup;
@property (strong, nonatomic) XXTTextField *textField;
@property (strong, nonatomic) XXTTextView *textView;

@end
