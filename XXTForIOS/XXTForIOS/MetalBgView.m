//
//  MetalBgView.m
//  XXTForIOS
//
//  Created by pandara on 13-12-2.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "MetalBgView.h"

@implementation MetalBgView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = [UIImage imageNamed:@"bg_texture.png"];
    [image drawAsPatternInRect:rect];
}

@end
