//
//  UIBubbleTableViewCell.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <QuartzCore/QuartzCore.h>
#import "UIBubbleTableViewCell.h"
#import "NSBubbleData.h"

@interface UIBubbleTableViewCell ()

@property (nonatomic, retain) UIView *customView;
@property (nonatomic, retain) UIImageView *bubbleImage;
//@property (nonatomic, retain) UIImageView *avatarImage;
@property (strong, nonatomic) UIButton *avatarButton;
@property (strong, nonatomic) UIActivityIndicatorView *juhuaView;

- (void) setupInternalData;

@end

@implementation UIBubbleTableViewCell

@synthesize data = _data;
@synthesize customView = _customView;
@synthesize bubbleImage = _bubbleImage;
@synthesize showAvatar = _showAvatar;
//@synthesize avatarImage = _avatarImage;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}

#if !__has_feature(objc_arc)
- (void) dealloc
{
    self.data = nil;
    self.customView = nil;
    self.bubbleImage = nil;
    self.avatarImage = nil;
    [super dealloc];
}
#endif

- (void)setDataInternal:(NSBubbleData *)value
{
	self.data = value;
	[self setupInternalData];
}

- (void) setupInternalData
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!self.bubbleImage)
    {
#if !__has_feature(objc_arc)
        self.bubbleImage = [[[UIImageView alloc] init] autorelease];
#else
        self.bubbleImage = [[UIImageView alloc] init];        
#endif
        [self addSubview:self.bubbleImage];
    }
    
    NSBubbleType type = self.data.type;
    
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;

    CGFloat x = (type == BubbleTypeSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    CGFloat y = 0;
    
    // Adjusting the x coordinate for avatar
    if (self.showAvatar)
    {
        [self.avatarButton removeFromSuperview];

        CGFloat avatarSize = 40;
        CGFloat avatarMargin = 7;
        
        self.avatarButton = [[UIButton alloc] init];
        [self.avatarButton setImageWithURL:[NSURL URLWithString:self.data.avatarURLStr] placeholder:[UIImage imageNamed:@"avatar_default_rect.png"]];
        CGFloat avatarX = (type == BubbleTypeSomeoneElse) ? avatarMargin : self.frame.size.width - avatarMargin - avatarSize;
        CGFloat avatarY = 0;
        
        self.avatarButton.frame = CGRectMake(avatarX, avatarY, avatarSize, avatarSize);
        [self addSubview:self.avatarButton];
        
//        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
//        if (delta > 0) y = delta;
        
        if (type == BubbleTypeSomeoneElse) x += 54;
        if (type == BubbleTypeMine) x -= 54;
    }

    [self.customView removeFromSuperview];
    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + self.data.insets.left, y + self.data.insets.top, width, height);
    [self.contentView addSubview:self.customView];

    if (type == BubbleTypeSomeoneElse)
    {
        self.bubbleImage.image = [[UIImage imageNamed:@"bubble_else.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 12, 10, 8) resizingMode:UIImageResizingModeStretch];
    }
    else {
        self.bubbleImage.image = [[UIImage imageNamed:@"bubble_me.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 8, 10, 12) resizingMode:UIImageResizingModeStretch];
    }

    self.bubbleImage.frame = CGRectMake(x, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom);
    //菊花
    if (self.data.isSending) {
        if (!self.juhuaView) {
            self.juhuaView = [[UIActivityIndicatorView alloc] init];
            self.juhuaView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        }
        
        CGFloat juhuaMargin = 15;
        if (type == BubbleTypeMine) {
            self.juhuaView.center = CGPointMake(x - juhuaMargin, self.bubbleImage.frame.size.height / 2);
        } else {
            self.juhuaView.center = CGPointMake(x + juhuaMargin, self.bubbleImage.frame.size.height / 2);
        }
        [self addSubview:self.juhuaView];
        [self.juhuaView startAnimating];
    } else {
        [self.juhuaView removeFromSuperview];
    }
}

@end
