//
//  Defines.h
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#ifndef XXTForIOS_Defines_h
#define XXTForIOS_Defines_h

#import "AppDelegate.h"

typedef void (^VoidBlock)(void);
typedef void (^StringBlock)(NSString *str);
typedef void (^DateBlock)(NSDate *date);
typedef void (^DictBlock)(NSDictionary *dict);
typedef void (^ArrayBlock)(NSArray *array);

#if 1 // Set to 1 to enable debug logging
#define XXTLog(x, ...) NSLog(x, ## __VA_ARGS__)
#else
#define XXTLog(x, ...) ;
#endif

#define nilObject(o) (NSNull *)o == [NSNull null]? nil:o
#define emptyStr(s) s == nil? @"":s

//size
#define DEVICE_BOUNDS [[UIScreen mainScreen] applicationFrame]
#define DEVICE_SIZE [[UIScreen mainScreen] applicationFrame].size
#define WINDOW_SIZE CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height + 20)
#define TABBAR_HEIGHT 49
#define NAVIGATIONBAR_HEIGHT 44

#define APP_NAVIGATIONBAR [[(AppDelegate *)[[UIApplication sharedApplication] delegate] navigationController] navigationBar]
#define APP_NAVIGATIONBAR_FIRSTITEM [APP_NAVIGATIONBAR.items objectAtIndex:0]

//tool
#define DEVICE_OS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define color(r, g, b, a) [UIColor colorWithRed:(CGFloat)r/255.0f green:(CGFloat)g/255.0f blue:(CGFloat)b/255.0f alpha:a]

//identify
#define XXTTABLECELL_ID @"XXTCell"

#define LOADING_ANIMATION_DUR 1.0f

//DES
#define DEFAULT_KEY @"JojoChat"

//URL
#define BASE_URL @"http://xxtforphp.sinaapp.com/"

//layout
#define BASEBACKGROUNDCOLOR color(225, 225, 225, 1)
#define TABLESEPARATORCOLOR color(150, 150, 150, 1)

//keys
#define kKeyChainServicePasswd @"passwd"
#define kKeyChainServiceAuth @"auth"

#define kUDAccount @"account"
#define kUDPasswd @"passwd"
#define kUDInfoDict @"infoDict"

//#define kInfoDictAccid @"accid"
//#define kInfoDict @"userName"
//#define kInfoDictAvatarURL @"avatarURL"
//#define kInfoDictCHNUserName @"chnUserName"
//#define kInfoDictTitle @"title"
//#define kInfoDictTime @"time"
//#define kInfoDictClientID @"clientID"
#define kInfoDictName @"name"
#define kInfoDictDegree @"degree"
#define kInfoDictAvatarURL @"avatarurl"
#define kInfoDictNick @"nick"

//#define kRequestID @"id"
//#define kRequestUSR @"usr"
//#define kRequestURL @"url"
//#define kRequestChn @"chn"
//#define kRequestTitle @"title"
//#define kRequestTime @"time"
//#define kRequestDate @"date"
//#define kRequestPid @"pid"
//#define kRequestHits @"hits"
//#define kRequestDate @"date"
//#define kRequestCont @"cont"
//#define kRequestRdate @"rdate"
//#define kRequestRtime @"rtime"
//#define kRemindDate @"reminddate"
#define kRequestError @"error"
#define kRequestMsg @"msg"
#define kRequestData @"data"
#define kRequestMAuth @"mauth"
#define kRequestNick @"nick"
#define kRequestDegree @"degree"
#define kRequestName @"name"
#define kRequestPasswd @"passwd"
#define kRequestAlarmDateLine @"alarmdateline"
#define kRequestOption @"option"
#define kRequestContent @"content"
#define kRequestCreateDateLine @"createdateline"
#define kRequestDateLine @"dateline"
#define kRequestHeat @"heat"
#define kRequestTitle @"title"
#define kRequestAlarmDay @"alarmday"
#define kRequestFromName @"fromname"
#define kRequestFromUser @"fromuser"
#define kRequestToUser @"touser"
#define kRequestToName @"toname"
#define kRequestChatList @"chatlist"
#define kRequestToUserInfo @"touserinfo"
#define kRequestAvatarURL @"avatarurl"
#define kRequestID @"id"
#define kRequestIntroduce @"introduce"
#define kRequestDiscourse @"discourse"
#define kRequestProgram @"program"
#define kRequestSchool @"school"
#define kRequestEmail @"email"

#define kLocalCacheChatID @"localcachechatid"
#define kLocalCacheChatSending @"localcachechatsending"

#define kNotifiUserInfoAlarmDate @"alarmDate"
#define kNotifiUserInfoContent @"content"
#define kNotifiUserInfoID @"notifiid"

#define kUserDefaultFriendList @"friendlist"
#define kUserDefaultFriendFeedList @"friendfeedlist"
#define kUserDefaultMyFeedList @"myfeedlist"
#define kUserDefaultCalendarList @"calendarlist"
#define kUserDefaultUserInfoDict @"userinfodict"
#define kUserDefaultHasLogined @""
#define kUserDefaultChatList @"chatlist"

#define kAppDictFriendList @"friendlist"

#define NOTIFI_RELOAD_FRIENDINFORELATED_CON @"reloadfriendinforelatedecon"
#define NOTIFI_REFRESH_ALL_CON @"refleshallcon"
#define NOTIFI_POSTCHATFINISH @"postchatfinish"
#define NOTIFI_NEW_CALENDAR @"newcalendar"

#define kNotifiData @"notifidata"
#define kCellIDFriendFeedCell @"FeedCell"
#define kCellIDCalendarCell @"calendarcell"

//tags
#define kViewTagATLabel 10086

//files
#define kFileFriendListCache @"friendlist.plist"
#define kFileChatListCache @"chatlistcache"
#define kFileAllChatListCache @"allchatlist"

//menthod
#define PRESENT_VIEWCONTROLLER(__PRESENTED_CONTROLLER__, __PRESENTING_CONTROLLER__, __ANIMATION__) \
if ([__PRESENTING_CONTROLLER__ respondsToSelector:@selector(presentViewController:animated:completion:)]) { \
    [__PRESENTING_CONTROLLER__ presentViewController:__PRESENTED_CONTROLLER__ animated:__ANIMATION__ completion:^{}];\
} else { \
    [__PRESENTING_CONTROLLER__ presentModalViewController:__PRESENTED_CONTROLLER__ animated:__ANIMATION__]; \
}

#define DISMISS_VIEWCONTROLLER(__CONTROLLER__, __ANIMATION__) \
if ([__CONTROLLER__ respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) { \
    [__CONTROLLER__ dismissModalViewControllerAnimated:__ANIMATION__]; \
} else { \
    [__CONTROLLER__ dismissViewControllerAnimated:__ANIMATION__ completion:^{ \
\
}]; \
}

#endif
