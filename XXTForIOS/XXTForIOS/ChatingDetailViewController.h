//
//  ChatingDetailViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-9-23.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableView.h"
#import "SSTextView.h"

@interface ChatingDetailViewController : UIViewController <UIBubbleTableViewDataSource, UIBubbleTableViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) NSString *toUserName;
@property (strong, nonatomic) NSString *toUserNick;
@property (strong, nonatomic) IBOutlet SSTextView *textView;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;
@property (assign, nonatomic) int currentPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withToUserName:(NSString *)toUserName andToUserNick:(NSString *)toUserNick;

@end
