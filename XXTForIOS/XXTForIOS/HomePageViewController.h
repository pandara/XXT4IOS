//
//  HomePageViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-5-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXTViewController.h"


@interface HomePageViewController : XXTViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
