//
//  MyFeedViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "MyFeedViewController.h"

@interface MyFeedViewController ()

@end

@implementation MyFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isMyFeedView = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = @"我的动态";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
