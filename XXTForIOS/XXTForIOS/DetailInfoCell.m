//
//  DetailInfoCell.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "DetailInfoCell.h"

@implementation DetailInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"DetailInfoCell" owner:self options:nil] objectAtIndex:0];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataWithContentText:(NSString *)contentText
{
    self.label.text = contentText;
}

@end
