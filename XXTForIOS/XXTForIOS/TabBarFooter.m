//
//  TapBarFooter.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "TabBarFooter.h"
#import "TabButton.h"

@implementation TabBarFooter

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"TabBarFooter" owner:self options:nil] objectAtIndex:0];
        
        //tabButton
        TabButton *scholButton = [[TabButton alloc] initWithImage:[UIImage imageNamed:@"tab_schol.png"] highlightedImage:[UIImage imageNamed:@"tab_schol_a.png"] withTitle:@"SCHOL@"];
        [scholButton setOrigin:CGPointMake(0, 0)];
        [scholButton setImageInsetWithTop:0 andLeft:5];
        
        TabButton *friendButton = [[TabButton alloc] initWithImage:[UIImage imageNamed:@"tab_friend.png"] highlightedImage:[UIImage imageNamed:@"tab_friend_a.png"] withTitle:@"好友"];
        [friendButton setOrigin:CGPointMake(scholButton.frame.size.width, 0)];
        
        TabButton *feedButton = [[TabButton alloc] initWithImage:[UIImage imageNamed:@"tab_feed.png"] highlightedImage:[UIImage imageNamed:@"tab_feed_a.png"] withTitle:@"动态"];
        [feedButton setOrigin:CGPointMake(friendButton.frame.origin.x + friendButton.frame.size.width, 0)];
        
        TabButton *settingButton = [[TabButton alloc] initWithImage:[UIImage imageNamed:@"tab_settings.png"] highlightedImage:[UIImage imageNamed:@"tab_settings_a.png"] withTitle:@"设置"];
        [settingButton setOrigin:CGPointMake(feedButton.frame.origin.x + feedButton.frame.size.width, 0)];
        
        [scholButton setHandlerBlockWhenTap:^{
            selectedIndex = 0;
            
            if (block != nil) {
                block(0);
            }
            
            [friendButton setState:TabButtonStateNormal];
            [feedButton setState:TabButtonStateNormal];
            [settingButton setState:TabButtonStateNormal];
        }];
        
        [friendButton setHandlerBlockWhenTap:^{
            selectedIndex = 1;
            
            if (block != nil) {
                block(1);
            }
            
            [scholButton setState:TabButtonStateNormal];
            [feedButton setState:TabButtonStateNormal];
            [settingButton setState:TabButtonStateNormal];
        }];
        
        [feedButton setHandlerBlockWhenTap:^{
            selectedIndex = 2;
            
            if (block != nil) {
                block(2);
            }
            
            [scholButton setState:TabButtonStateNormal];
            [friendButton setState:TabButtonStateNormal];
            [settingButton setState:TabButtonStateNormal];
        }];
        
        [settingButton setHandlerBlockWhenTap:^{
            selectedIndex = 3;
            
            if (block != nil) {
                block(3);
            }
            
            [scholButton setState:TabButtonStateNormal];
            [friendButton setState:TabButtonStateNormal];
            [feedButton setState:TabButtonStateNormal];
        }];
        
        [self addSubview:scholButton];
        [self addSubview:friendButton];
        [self addSubview:feedButton];
        [self addSubview:settingButton];
        
        self.buttonArray = @[scholButton, friendButton, feedButton, settingButton];
    }
    return self;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (void)setSelectedIndex:(int)buttonIndex
{   
    TabButton *currentButton = (TabButton *)[self.buttonArray objectAtIndex:selectedIndex];
    [currentButton setState:TabButtonStateNormal];
    
    selectedIndex = buttonIndex;
    currentButton = (TabButton *)[self.buttonArray objectAtIndex:selectedIndex];
    [currentButton setState:TabButtonStateHighLight];
    
    if (block != nil) {
        block(selectedIndex);
    }
}

- (void)setHandleBlock:(TabBarFooterBlock)handleBlock
{
    block = handleBlock;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
