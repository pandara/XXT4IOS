//
//  TapButton.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "TabButton.h"

@implementation TabButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage withTitle:(NSString *)title
{
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"TabButton" owner:self options:nil] objectAtIndex:0];
        
        self.normalImage = image;
        self.highLightImage = highlightedImage;
        self.imageView.image = image;
        self.titleLabel.text = title;
        
        //添加手势
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTheView:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)tapTheView:(id)sender
{
    [self setState:TabButtonStateHighLight];
    
    if (block != nil) {
        block();
    }
}

- (void)setState:(TabButtonState)state
{
    switch (state) {
        case TabButtonStateNormal:
        {
            self.imageView.image = self.normalImage;
            self.highLightView.hidden = YES;
            self.titleLabel.textColor = color(191, 191, 191, 1);
        }
            break;
        case TabButtonStateHighLight:
        {
            self.imageView.image = self.highLightImage;
            self.highLightView.hidden = NO;
            self.titleLabel.textColor = color(255, 255, 255, 1);
        }
            break;
        default:
            break;
    }
}

- (void)setHandlerBlockWhenTap:(TabButtonBlock)handlerBlock
{
    block = [handlerBlock copy];
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (void)setImageInsetWithTop:(CGFloat)top andLeft:(CGFloat)left
{
    CGRect frame = self.imageView.frame;
    frame.origin.x -= left;
    frame.origin.y -= top;
    self.imageView.frame = frame;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
