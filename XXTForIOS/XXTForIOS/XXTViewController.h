//
//  XXTViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-11-26.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XXTViewController : UIViewController

@property (strong, nonatomic) UIActivityIndicatorView *juhua;

- (void)doneLoading;
- (void)startLoading;

- (void)reloadData;

@end
