//
//  FeedBackViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-3.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTViewController.h"
#import "XXTSlideTextView.h"

@interface FeedBackViewController : XXTViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet XXTSlideTextView *textView;

@end
