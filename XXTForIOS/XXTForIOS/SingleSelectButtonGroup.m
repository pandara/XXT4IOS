//
//  SingleSelectButtonGroup.m
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SingleSelectButtonGroup.h"

@interface SingleSelectButtonGroup()

@property (strong, nonatomic) NSArray *buttonArray;

@end

@implementation SingleSelectButtonGroup

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"SingleSelectButtonGroup" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.frame = frame;
        _bgImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        
        _buttonArray = @[_firstButton, _secondButton, _thirdButton];
        _bgImageView.image = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg.png"] withInsetTop:10 left:10 bottom:10 right:10];
        [self setSelectedButtonWithIndex:_selectedIndex];
    }
    return self;
}

- (void)setButton:(UIButton *)button withSelectState:(BOOL)selected
{
    if (selected) {
        [button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    } else {
        [button setImage:[UIImage imageNamed:@"notChecked.png"] forState:UIControlStateNormal];
    }
}

- (void)setSelectedButtonWithIndex:(int)index
{
    _selectedIndex = index;
    for (int i = 0; i < [_buttonArray count]; i++) {
        if (i != index) {
            [self setButton:[_buttonArray objectAtIndex:i] withSelectState:NO];
        }
    }
    
    [self setButton:[_buttonArray objectAtIndex:index] withSelectState:YES];
}

- (IBAction)pressFirstButton:(id)sender {
    [self setSelectedButtonWithIndex:0];
}

- (IBAction)pressSecondButton:(id)sender {
    [self setSelectedButtonWithIndex:1];
}

- (IBAction)pressThirdButton:(id)sender {
    [self setSelectedButtonWithIndex:2];
}


@end
