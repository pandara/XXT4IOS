//
//  CalendarCell.m
//  XXTForIOS
//
//  Created by pandara on 13-11-29.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "CalendarCell.h"

@implementation CalendarCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"CalendarCell" owner:self options:nil] objectAtIndex:0];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataWithTitle:(NSString *)title
              createTime:(NSString *)createTime
              remindTime:(NSString *)remindTime
               isExpired:(BOOL)isExpired
{
    if (isExpired) {
        //122 208 0
        _remindLabel.textColor = color(122, 208, 0, 1);
        _remindLabel.text = @"已过期";
    } else {
        _remindLabel.textColor = color(255, 0, 0, 1);
        _remindLabel.text = [NSString stringWithFormat:@"提醒时间: %@", remindTime];
    }
    
    _theTimeLabel.text = createTime;
    _theTitleLabel.text = title;
}

@end
