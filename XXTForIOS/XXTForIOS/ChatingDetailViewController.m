//
//  ChatingDetailViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-23.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "ChatingDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "XXTHttpClient.h"
#import "DetailInfoViewController.h"

@interface ChatingDetailViewController () {
    int _sendingID;//正在发送的对话的本地ID
}

@property (strong, nonatomic) UIBubbleTableView *tableView;
@property (strong, nonatomic) NSMutableArray *chatArray;
@property (strong, nonatomic) NSDictionary *toUserDict;
@property (strong, nonatomic) NSDictionary *fromUserDict;
@property (strong, nonatomic) NSTimer *pullTimer;
@property (strong, nonatomic) NSMutableArray *sendingChatArray;

@end

@implementation ChatingDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withToUserName:(NSString *)toUserName andToUserNick:(NSString *)toUserNick
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.toUserName = toUserName;
        self.toUserNick = toUserNick;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //send button
    UIImage *normalImage;
    const float edgeInset = 5;
    if (DEVICE_OS_VERSION < 6.0f) {
        normalImage = [[UIImage imageNamed:@"btn_white.png"] stretchableImageWithLeftCapWidth:edgeInset topCapHeight:edgeInset];
    } else {
        normalImage = [[UIImage imageNamed:@"btn_white.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(edgeInset, edgeInset, edgeInset, edgeInset)];
    }
    
    [self.sendButton setBackgroundImage:normalImage forState:UIControlStateNormal];
    
    //info button
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mm_title_btn_contact_normal.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(pressDetailButton:)];

    self.navigationItem.rightBarButtonItem = rightItem;
    
    //textView
    self.textView.layer.cornerRadius = 3.0f;
    self.textView.layer.shadowColor = color(0, 0, 0, 1).CGColor;
    self.textView.layer.shadowOffset = CGSizeMake(0, 0);
    self.textView.layer.shadowRadius = 5.0f;
    self.textView.layer.shadowOpacity = 1;
    
    //tableivew
    self.tableView = [[UIBubbleTableView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height - 44 - 44)];
    self.tableView.bubbleDataSource = self;
    self.tableView.bubbleDelegate = self;
    self.tableView.showAvatars = YES;
    self.tableView.showsVerticalScrollIndicator = YES;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTheTableView:)];
    [self.tableView addGestureRecognizer:tapGesture];
    
    //data
    self.chatArray = [[NSMutableArray alloc] init];
    
    NSDictionary *cacheDataDict = [XXTToolKit getDataFromChatListCacheWithName:self.toUserName];
    [self.chatArray addObjectsFromArray:[cacheDataDict objectForKey:kRequestChatList]];
    self.fromUserDict = [cacheDataDict objectForKey:kRequestFromUser];
    self.toUserDict = [cacheDataDict objectForKey:kRequestToUser];
    
    [self.tableView reloadData];
    [self scrollTableToBottomAnimate:NO];
    
    [self requestChatListWithPage:0 perpage:10 completeBlock:^(NSArray *array){
        [self.chatArray removeAllObjects];
        [self.chatArray addObjectsFromArray:array];
        [self.chatArray addObjectsFromArray:self.sendingChatArray];
        [self.tableView reloadData];
        [self scrollTableToBottomAnimate:YES];
        
        [self saveDataToCache];
        
        //每3秒拉一次
        self.pullTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(pullChatList) userInfo:nil repeats:YES];
    }];
    
    //标题
    self.navigationItem.title = [XXTToolKit getNameFromNick:self.toUserNick andName:self.toUserName];
    
    //返回按钮
    UINavigationItem *item = [self.navigationController.navigationBar.items objectAtIndex:0];
    
    //如果返回键的title是scholat，需要手动调整一下
    NSString *title = item.title;
    if ([title rangeOfString:@"SCHOL"].location != NSNotFound) {
        item.title = @"SCHOL@";
    }
    
//    [self scrollTableToBottomAnimate:NO];
    
}

- (void)pressDetailButton:(id)sender
{
    DetailInfoViewController *con = [[DetailInfoViewController alloc] initWithNibName:@"DetailInfoViewController" bundle:nil targetName:self.toUserName];
    [self.navigationController pushViewController:con animated:YES];
}

- (void)saveDataToCache
{
    [XXTToolKit setDataToChatListCache:self.chatArray fromUserInfo:self.fromUserDict toUserInfo:self.toUserDict];
}

- (void)tapTheTableView:(UITapGestureRecognizer *)tapGesture
{
    [self.textView resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.pullTimer) {
        [self.pullTimer fire];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFI_POSTCHATFINISH object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postChatFinish:) name:NOTIFI_POSTCHATFINISH object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFI_POSTCHATFINISH object:nil];
    [self.pullTimer invalidate];
}

- (void)postChatFinish:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    int sendID = [[userInfo objectForKey:kLocalCacheChatID] intValue];
    [self setSendingFinishWithSendID:sendID];
    [self.tableView reloadData];
}

- (void)setSendingFinishWithSendID:(int)sendID
{
    int i = 0;
    for (NSDictionary *sendingChatInfo in self.chatArray) {
        if ([[sendingChatInfo objectForKey:kLocalCacheChatID] intValue] == sendID) {
            NSMutableDictionary *mutableSendChatInfo = [[NSMutableDictionary alloc] init];
            [mutableSendChatInfo addEntriesFromDictionary:sendingChatInfo];
            [mutableSendChatInfo setObject:@"0" forKey:kLocalCacheChatSending];
            [self.chatArray replaceObjectAtIndex:i withObject:mutableSendChatInfo];
            break;
        }
        i++;
    }
}

- (void)deleteItemInSendingChatArrayWithSendID:(int)sendID
{
    int i = 0;
    for (NSDictionary *sendingChatInfo in self.sendingChatArray) {
        if ([[sendingChatInfo objectForKey:kLocalCacheChatID] intValue] == sendID) {
            [self.sendingChatArray removeObjectAtIndex:i];
            break;
        }
        i++;
    }
}

- (void)deleteItemInChatArrayWithSendID:(int)sendID
{
    int i = 0;
    for (NSDictionary *chatInfo in self.chatArray) {
        if ([[chatInfo objectForKey:kLocalCacheChatID] intValue] == sendID) {
            [self.chatArray removeObjectAtIndex:i];
            return;
        }
        
        i++;
    }
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [XXTToolKit setView:self.bottomBar toOriginY:self.view.frame.size.height - self.bottomBar.frame.size.height - keyBoardHeight];
        [XXTToolKit setView:self.tableView toSizeH:self.view.frame.size.height - self.bottomBar.frame.size.height - keyBoardHeight];
    } completion:^(BOOL finished) {
        [self scrollTableToBottom];
    }];
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    CGFloat offsetY = MAX(self.tableView.contentSize.height - self.tableView.frame.size.height, 0);
    [XXTToolKit setView:self.tableView toSizeH:self.view.frame.size.height - self.bottomBar.frame.size.height];
    self.tableView.contentOffset = CGPointMake(0, offsetY);
    
    [UIView animateWithDuration:animationDuration animations:^{
        [XXTToolKit setView:self.bottomBar toOriginY:self.view.frame.size.height - self.bottomBar.frame.size.height];
        self.tableView.contentOffset = CGPointMake(0, MAX(0, _tableView.contentSize.height - _tableView.frame.size.height));
    } completion:^(BOOL finished) {
        //        [self scrollTableToBottom];
    }];
}

- (void)pullChatList
{
    XXTLog(@"我拉");
    [self requestChatListWithPage:0 perpage:[self.chatArray count] completeBlock:^(NSArray *array){
        if ([array count] == 0) {
            return;
        }
        if ([self.chatArray count] > 0) {
            if ([[[self.chatArray objectAtIndex:0] objectForKey:kRequestID] isEqualToString:[[array objectAtIndex:0] objectForKey:kRequestID]]) {
                return;
            }
        }
        [self.chatArray removeAllObjects];
        [self.chatArray addObjectsFromArray:[array subarrayWithRange:NSMakeRange(0, MIN([array count], 10))]];//接收到新消息，会滑动到底部，趁机将chatArray长度控制回来
        [self.chatArray addObjectsFromArray:self.sendingChatArray];
        [self.tableView reloadData];
        
        [self saveDataToCache];
        
        [self scrollTableToBottom];
    }];
}

//滚动tableview到底部
- (void)scrollTableToBottom
{
    [self scrollTableToBottomAnimate:YES];
}

- (void)scrollTableToBottomAnimate:(BOOL)animate
{
    CGFloat y = MAX(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
    [self.tableView scrollRectToVisible:CGRectMake(0, y, DEVICE_SIZE.width, self.tableView.frame.size.height) animated:animate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSendButton:nil];
    [self setTextView:nil];
    [self setTextView:nil];
    [self setBottomBar:nil];
    [super viewDidUnload];
}

- (IBAction)pressSendButton:(id)sender {
    
    //本地发送假快
    NSTimeInterval dateLine = [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970];
    int cacheID = [self getLocalCacheChatID];
    
    NSDictionary *localCacheChatInfo = @{
                                         kLocalCacheChatID: [NSString stringWithFormat:@"%d", cacheID],
                                         kLocalCacheChatSending: @"1",
                                         kRequestFromName: [XXTToolKit getName],
                                         kRequestToName: self.toUserName,
                                         kRequestContent: self.textView.text,
                                         kRequestDateLine:[NSString stringWithFormat:@"%f", dateLine],
                                         };
    
    if (!self.sendingChatArray) {
        self.sendingChatArray = [[NSMutableArray alloc] init];
    }
    
    [self.sendingChatArray addObject:localCacheChatInfo];
    [self.chatArray addObject:localCacheChatInfo];
    [self.tableView reloadData];
    [self scrollTableToBottom];
    
    [self requestToPostChatWithContent:self.textView.text errorBlock:^(NSString *str) {
        [self deleteItemInSendingChatArrayWithSendID:cacheID];
        [self deleteItemInChatArrayWithSendID:cacheID];
        [self.tableView reloadData];
        [self scrollTableToBottom];
    } completeBlock:^{
        [self deleteItemInSendingChatArrayWithSendID:cacheID];
        [self setSendingFinishWithSendID:cacheID];
        [self.tableView reloadData];
        [self scrollTableToBottom];
        
        [self saveDataToCache];
    }];
    
    self.textView.text = @"";
}

- (int)getLocalCacheChatID {
    _sendingID++;
    return _sendingID;
}

#pragma mark - BubbleTableViewDataSource, BubbleTableViewDelegate
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [self.chatArray count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    NSDictionary *chatInfoDict = [self.chatArray objectAtIndex:row];
    
    NSBubbleType bubbleType;
    NSString *avatarURLStr;
    if ([[chatInfoDict objectForKey:kRequestFromName] isEqualToString:[XXTToolKit getName]]) {
        bubbleType = BubbleTypeMine;
        avatarURLStr = [self.fromUserDict objectForKey:kRequestAvatarURL];
    } else {
        bubbleType = BubbleTypeSomeoneElse;
        avatarURLStr = [self.toUserDict objectForKey:kRequestAvatarURL];
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[chatInfoDict objectForKey:kRequestDateLine] doubleValue]];
    
    NSBubbleData *bubbleData = [NSBubbleData dataWithText:[chatInfoDict objectForKey:kRequestContent] date:date type:bubbleType];
    bubbleData.avatarURLStr = avatarURLStr;
    bubbleData.isSending = [[chatInfoDict objectForKey:kLocalCacheChatSending] intValue];
    
    return bubbleData;
}

- (void)bubbleTableViewToLoadMore:(UIBubbleTableView *)bubbleTableView
{
    [self requestChatListWithPage:self.currentPage + 1 perpage:10 completeBlock:^(NSArray *array) {
        self.currentPage++;
        [self.chatArray addObjectsFromArray:array];
        [self.tableView reloadData:YES];
    }];
}

#pragma mark - URLRequest
- (void)requestChatListWithPage:(int)page perpage:(int)perpage completeBlock:(ArrayBlock)completeBlock
{
    NSString *url = [NSString stringWithFormat:@"%@?op=chatlist&mauth=%@&name=%@&toname=%@&page=%d&perpage=%d", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName], self.toUserName, page, perpage];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(NSDictionary *responObject) {
        
        NSDictionary *dataDict = [responObject objectForKey:kRequestData];
        if (!self.fromUserDict) {
            self.fromUserDict = [dataDict objectForKey:kRequestFromUser];
        }
        if (!self.toUserDict) {
            self.toUserDict = [dataDict objectForKey:kRequestToUser];
        }
        
        
        
        if (completeBlock) {
            completeBlock([dataDict objectForKey:kRequestChatList]);
        }
    } failBlock:^{
        
    }];
}

- (void)requestToPostChatWithContent:(NSString *)content errorBlock:(StringBlock)errorBlock completeBlock:(VoidBlock)completeBlock
{
    NSString *url = [NSString stringWithFormat:@"%@?op=postchat", BASE_URL];
    NSDictionary *para = @{
                           kRequestMAuth: [XXTToolKit getMAuth],
                           kRequestName: [XXTToolKit getName],
                           kRequestContent: content,
                           kRequestToName: self.toUserName,
                           };
    
    
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:para successBlock:^(NSDictionary *responObject) {
        if (completeBlock) {
            completeBlock();
        }
    } errorBlock:^(NSString *str) {
        if (errorBlock) {
            errorBlock(str);
        }
    } failBlock:^{
        
    }];
    
}

@end























