//
//  DetailInfoViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "DetailInfoViewController.h"
#import "DetailInfoCell.h"
#import "DetailInfoTableHead.h"
#import "XXTHttpClient.h"
#import <QuartzCore/QuartzCore.h>

@interface DetailInfoViewController ()

@property (strong, nonatomic) NSMutableDictionary *detailInfoDict;
@property (strong, nonatomic) DetailInfoTableHead *tableHead;

@end

@implementation DetailInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil targetName:(NSString *)targetName
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.targetName = targetName;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.detailInfoDict = [[NSMutableDictionary alloc] init];
    
    self.view.backgroundColor = color(229, 229, 229, 1);
    
    self.tableHead = [[DetailInfoTableHead alloc] init];
    self.tableView.tableHeaderView = self.tableHead;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.navigationItem.title = @"学者信息";
    
    [self requestToGetDetailInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

#pragma mark - UItableViewDelegate UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    header.backgroundColor = color(229, 229, 229, 1);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 40)];
    label.backgroundColor = color(246, 204, 0, 1);
    label.textColor = [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:21];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.cornerRadius = 3.0f;
    
    switch (section) {
        case 0:
        {
            label.text = @"个人简介";
        }
            break;
        case 1:
        {
            label.text = @"学术论文";
        }
            break;
        case 2:
        {
            label.text = @"科研项目";
        }
            break;
        default:
            break;
    }
    
    [header addSubview:label];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *contentString;
    switch (indexPath.section) {
        case 0:
        {
            contentString = emptyStr([self.detailInfoDict objectForKey:kRequestIntroduce]);
        }
            break;
        case 1:
        {
            contentString = emptyStr([self.detailInfoDict objectForKey:kRequestDiscourse]);
        }
            break;
        case 2:
        {
            contentString = emptyStr([self.detailInfoDict objectForKey:kRequestProgram]);
        }
            break;
        default:
            break;
    }
    
    return [contentString sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(320, 10000)].height + 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[DetailInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    switch (indexPath.section) {
        case 0:
        {
            [cell setDataWithContentText:emptyStr([self.detailInfoDict objectForKey:kRequestIntroduce])];
        }
            break;
        case 1:
        {
            [cell setDataWithContentText:emptyStr([self.detailInfoDict objectForKey:kRequestDiscourse])];
        }
            break;
        case 2:
        {
            [cell setDataWithContentText:emptyStr([self.detailInfoDict objectForKey:kRequestProgram])];
        }
            break;
        default:
            break;
    }
    
    return cell;
}

#pragma mark - URLRequest
- (void)requestToGetDetailInfo
{
    NSString *url = [NSString stringWithFormat:@"%@?op=detailinfo&mauth=%@&name=%@&targetname=%@", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName], self.targetName];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(NSDictionary *responObject) {
        [self.detailInfoDict removeAllObjects];
        [self.detailInfoDict addEntriesFromDictionary:[responObject objectForKey:kRequestData]];
        XXTLog(@"self.detailInfoDict:%@", self.detailInfoDict);
        [self.tableView reloadData];
        
        [self.tableHead setDataWithShowedName:[XXTToolKit getNameFromNick:emptyStr([self.detailInfoDict objectForKey:kRequestNick])
                                                                  andName:emptyStr([self.detailInfoDict objectForKey:kRequestName])]
                                       degree:emptyStr([self.detailInfoDict objectForKey:kRequestDegree])
                                       school:emptyStr([self.detailInfoDict objectForKey:kRequestSchool])
                                        email:emptyStr([self.detailInfoDict objectForKey:kRequestEmail])
                                 avatarURLStr:emptyStr([self.detailInfoDict objectForKey:kRequestAvatarURL])];
    } failBlock:^{
        
    }];
}

@end
















