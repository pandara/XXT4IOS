//
//  XXTTableCell.h
//  XXTForIOS
//
//  Created by pandara on 13-9-13.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    XXTTableCellStyleWithTime,
    XXTTableCellStyleNoTime,
}XXTTableCellStyle;

@interface XXTTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;
@property (strong, nonatomic) IBOutlet UILabel *theTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *theSubTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *theTimeLabel;
@property (strong, nonatomic) IBOutlet UIView *bgView;

- (void)setStyle:(XXTTableCellStyle)style;
- (void)setImageURLStr:(NSString *)imageURLStr
                 title:(NSString *)title
              subTitle:(NSString *)subTitle
               dateStr:(NSString *)dateStr;
- (void)setImageURLStr:(NSString *)imageURLStr
                 title:(NSString *)title
              subTitle:(NSString *)subTitle;

@end
