//
//  HomePageViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-5-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "HomePageViewController.h"

@interface HomePageViewController ()

@end

@implementation HomePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startLoading];
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.scholat.com/~PandaraWen"]]];
    self.title = @"我的主页";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self doneLoading];
//    NSString *meta = [NSString stringWithFormat:@"\
//                      var meta = document.createElement(\"meta\");\
//                      meta.name = \"viewport\";\
//                      meta.content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\";\
//                      document.head.appendChild(meta);", webView.frame.size.width];
//    [webView stringByEvaluatingJavaScriptFromString:meta];
}

@end






