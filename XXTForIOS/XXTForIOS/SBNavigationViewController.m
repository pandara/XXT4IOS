//
//  SBNavigationViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "SBNavigationViewController.h"
#import "XXTViewController.h"

@interface SBNavigationViewController ()

@end

@implementation SBNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //设置背景
    self.navigationBar.tintColor = color(17, 118, 203, 1);
    
//    UIImage *navigationBarBackImage = [UIImage imageNamed:@"title_bar.png"];
//    if (DEVICE_OS_VERSION >= 5.0f) {
//        [self.navigationBar setBackgroundImage:navigationBarBackImage forBarMetrics:UIBarMetricsDefault];
//    } else {
//        UIImageView *backImageView = [[UIImageView alloc] initWithImage:navigationBarBackImage];
//        backImageView.frame = CGRectMake(0, 0, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height);
//        [self.navigationBar insertSubview:backImageView atIndex:1];
//    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
