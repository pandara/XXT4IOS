//
//  LoadingView.m
//  XXTForIOS
//
//  Created by pandara on 13-9-15.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView () {
    BOOL shouldStopRotation;
}

@end

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:self options:nil] objectAtIndex:0];
        shouldStopRotation = NO;
    }
    return self;
}

- (void)beginAnimation
{
//    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//        self.atLabel.transform = CGAffineTransformRotate(self.atLabel.transform, M_PI/2);
//    } completion:^(BOOL finished) {
//        if (!shouldStopRotation) {
//            [self beginAnimation];
//        }
//    }];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 4];
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 1.2f;
    rotationAnimation.repeatCount = 100;//动画执行两次
    [self.atLabel.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)stopAnimation
{
    [UIView animateWithDuration:0.6f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
        self.alpha = 0;
    } completion:^(BOOL finished) {
        shouldStopRotation = YES;
        [self removeFromSuperview];
    }];
}

@end
