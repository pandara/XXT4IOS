//
//  CalendarViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-11-29.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTViewController.h"

@interface CalendarViewController : XXTViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;

@end
