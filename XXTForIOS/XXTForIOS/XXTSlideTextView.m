//
//  XXTSlideTextView.m
//  XXTForIOS
//
//  Created by Pandara on 14-2-28.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTSlideTextView.h"

@implementation XXTSlideTextView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addSwipeGesture];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSwipeGesture];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self addSwipeGesture];
    }
    return self;
}

- (void)addSwipeGesture
{
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeTheTextView:)];
    gesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:gesture];
}

- (void)swipeTheTextView:(UISwipeGestureRecognizer *)gesture
{
    [self resignFirstResponder];
}

//- (void)willMoveToSuperview:(UIView *)newSuperview
//{
//    
//}
//
//- (void)didMoveToSuperview
//{
//    
//}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
