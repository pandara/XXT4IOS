//
//  CalendarViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-29.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "CalendarViewController.h"
#import "CalendarCell.h"
#import "XXTHttpClient.h"
#import "SVProgressHUD.h"
#import "PostCalendarViewController.h"


@interface CalendarViewController ()

@property (strong, nonatomic) NSMutableArray *calendarArray;
@property (strong, nonatomic) NSMutableArray *needRemindCalendarArray;

@end

@implementation CalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = BASEBACKGROUNDCOLOR;
    
    self.navigationItem.title = @"云端日历";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"新建" style:UIBarButtonItemStyleBordered target:self action:@selector(pressMoreButton:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _needRemindCalendarArray = [[NSMutableArray alloc] init];
    
    _calendarArray = [[NSMutableArray alloc] init];
    [_calendarArray addObjectsFromArray:[XXTToolKit getCalendarArray]];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    
    [self startLoading];
    [self requestToGetCalenderWithCompleteBlock:^{
        [self doneLoading];
        [_tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"日历同步成功"];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNewCalendar:) name:NOTIFI_NEW_CALENDAR object:nil];
}

- (void)receiveNewCalendar:(NSNotification *)notification
{
    [_calendarArray insertObject:notification.userInfo atIndex:0];
    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFI_NEW_CALENDAR object:nil];
}

- (void)pressMoreButton:(id)sender
{
    PostCalendarViewController *con = [[PostCalendarViewController alloc] initWithNibName:@"PostCalendarViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}

#pragma mark - UITableViewDelegate UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_calendarArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIDCalendarCell];
    if (cell == nil) {
        cell = [[CalendarCell alloc] init];
    }
    
    NSDictionary *calendarDict = [_calendarArray objectAtIndex:indexPath.row];
    NSString *title = [calendarDict objectForKey:kRequestContent];
    
    NSString *createDateLine = emptyStr([calendarDict objectForKey:kRequestCreateDateLine]);
    NSString *alarmDateLine = emptyStr([calendarDict objectForKey:kRequestAlarmDateLine]);
    BOOL isExpired = NO;
    if ([alarmDateLine doubleValue] <= [XXTToolKit getCurrentTimeInterval]) {
        isExpired = YES;
    }
    
    [cell setDataWithTitle:title
                createTime:[XXTToolKit getFormatDateStrFromDateLine:createDateLine]
                remindTime:[XXTToolKit getFormatDateStrFromDateLine:alarmDateLine]
                 isExpired:isExpired];
    
    return cell;
}

- (void)setCalendarNotificationFromArray:(NSArray *)remindArray
{
    //移除所有推送
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    //添加推送
    for (NSDictionary *calendarDict in remindArray) {
        UILocalNotification *notification = [[UILocalNotification alloc] init];

        if (notification != nil) {
            notification.fireDate = [calendarDict objectForKey:kNotifiUserInfoAlarmDate];
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.userInfo = calendarDict;
            notification.alertBody = [calendarDict objectForKey:kNotifiUserInfoContent];
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 1;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }

    }
}

#pragma mark - URLRequest
- (void)requestToGetCalenderWithCompleteBlock:(VoidBlock)block
{
    NSString *url = [NSString stringWithFormat:@"%@?op=calendarlist&mauth=%@&name=%@&page=0&perpage=100", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName]];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(NSDictionary *responObject) {
        [_calendarArray removeAllObjects];
        [_calendarArray addObjectsFromArray:[responObject objectForKey:kRequestData]];
        [XXTToolKit setCalendarArray:_calendarArray];
        
        NSMutableArray *needFireArray = [[NSMutableArray alloc] init];
        [_needRemindCalendarArray removeAllObjects];
        for (int i = 0; i < [_calendarArray count]; i++) {
            NSDictionary *calendarDict = [_calendarArray objectAtIndex:i];
            NSString *alarmDateLine = emptyStr([calendarDict objectForKey:kRequestAlarmDateLine]);
            NSString *alarmDate = [NSDate dateWithTimeIntervalSince1970:[alarmDateLine doubleValue]];
            
            if ([alarmDateLine doubleValue] > [XXTToolKit getCurrentTimeInterval]) {
                [needFireArray addObject:calendarDict];
                [_needRemindCalendarArray addObject:@{
                                        kNotifiUserInfoContent: emptyStr([calendarDict objectForKey:kRequestContent]),
                                        kNotifiUserInfoAlarmDate: alarmDate,
                                  kNotifiUserInfoID: [calendarDict objectForKey:kRequestCreateDateLine],
                 }];
                [self setCalendarNotificationFromArray:_needRemindCalendarArray];
            }
        }
        
        [_calendarArray removeObjectsInArray:needFireArray];
        for (int i = 0; i < [needFireArray count]; i++) {
            [_calendarArray insertObject:[needFireArray objectAtIndex:i] atIndex:0];
        }
        
        if (block) {
            block();
        }
    } failBlock:^{
        
    }];
}

@end
