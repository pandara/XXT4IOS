//
//  XXTHttpClient.h
//  XXTForIOS
//
//  Created by pandara on 13-9-18.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "JSONKit.h"

typedef void (^XXTHttpSuccessBlock)(NSDictionary *responObject);//responObject 为NSArray 或者 NSDictionary
typedef VoidBlock XXTHttpFailBlock;
typedef StringBlock XXTHttpErrorBlock;

@interface XXTHttpClient : NSObject

@property (strong, nonatomic) NSMutableArray *operationArray;

+ (XXTHttpClient *)sharedInstance;
- (void)requestWithURLStr:(NSString *)urlStr
                     para:(NSDictionary *)para
             successBlock:(XXTHttpSuccessBlock)successBlock
                failBlock:(XXTHttpFailBlock)failBlock;

- (void)requestWithURLStr:(NSString *)urlStr
                     para:(NSDictionary *)para
             successBlock:(XXTHttpSuccessBlock)successBlock
               errorBlock:(XXTHttpErrorBlock)errorBlock
                failBlock:(XXTHttpFailBlock)failBlock;

- (void)cancelAllOperation;

@end
