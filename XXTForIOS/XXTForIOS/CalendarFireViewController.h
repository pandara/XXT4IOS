//
//  CalendarFireViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-12-2.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTViewController.h"

@interface CalendarFireViewController : XXTViewController
@property (strong, nonatomic) IBOutlet UIImageView *handImageView;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@property (strong, nonatomic) UILocalNotification *notification;

@end
