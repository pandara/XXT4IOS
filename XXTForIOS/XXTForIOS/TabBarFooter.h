//
//  TapBarFooter.h
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TabBarFooterBlock)(int buttonIndex);

@interface TabBarFooter : UIView {
    TabBarFooterBlock block;
    int selectedIndex;
}

@property (strong, nonatomic) NSArray *buttonArray;

- (void)setOrigin:(CGPoint)origin;
- (void)setHandleBlock:(TabBarFooterBlock)handleBlock;
- (void)setSelectedIndex:(int)buttonIndex;

@end
