//
//  SplitView.h
//  XXTForIOS
//
//  Created by pandara on 13-9-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *leftImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rightImageView;

- (void)beginAnimation;

@end
