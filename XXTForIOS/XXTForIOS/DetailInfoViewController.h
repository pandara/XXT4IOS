//
//  DetailInfoViewController.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "XXTViewController.h"

@interface DetailInfoViewController : XXTViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSString *targetName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil targetName:(NSString *)targetName;

@end
