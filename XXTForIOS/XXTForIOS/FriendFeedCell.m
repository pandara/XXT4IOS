//
//  FriendFeedCell.m
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FriendFeedCell.h"
#import "UIImageView+JMImageCache.h"

#define TEXTINFO_MARGIN 5
#define HITBGIMAGE_MARGIN_BOTTOM 5
#define HITBGIMAGE_SIZE_H 40
#define TITLE_HIT_INTERVAL 20 //两个label的间距

@implementation FriendFeedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (CGFloat)hegihtFromTitle:(NSString *)title
{
    return 44 + [FlowLayoutLabel getHeightFromText:title maxWidth:218 maxLine:0 font:[UIFont systemFontOfSize:17] fontSize:17].height + TITLE_HIT_INTERVAL + HITBGIMAGE_SIZE_H - TEXTINFO_MARGIN * 2 + HITBGIMAGE_MARGIN_BOTTOM;
}

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"FriendFeedCell" owner:self options:nil] objectAtIndex:0];
    if (self) {
        _timeLabel.adjustsFontSizeToFitWidth = YES;
        
        if (DEVICE_OS_VERSION < 6.0f) {
            _titleBgImageView.image = [[UIImage imageNamed:@"dynamic_background.9.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:15];
            _hitBgImageView.image = [[UIImage imageNamed:@"hot_background.9.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:0];
        } else {
            _titleBgImageView.image = [[UIImage imageNamed:@"dynamic_background.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 25, 10, 10) resizingMode:UIImageResizingModeStretch];
            _hitBgImageView.image = [[UIImage imageNamed:@"hot_background.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 15, 5, 15) resizingMode:UIImageResizingModeStretch];
        }
        
//        _hitBgImageView.image = [UIImage imageNamed:@"hot_background.9.png"];
        [_titleLabel setMaxWidth:218 maxLine:0 font:[UIFont systemFontOfSize:17] fontSize:17];
        [_hitLabel setMaxWidth:320 maxLine:1 font:[UIFont systemFontOfSize:14] fontSize:14];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataWithAvatarURLStr:(NSString *)avatarURLStr name:(NSString *)name dateTimeStr:(NSString *)dateTimeStr title:(NSString *)title hit:(int)hit
{
    [_avatarImageView setImageWithURL:[NSURL URLWithString:avatarURLStr] placeholder:[UIImage imageNamed:@"avatar_default_rect.png"]];
    _nameLabel.text = name;
    _timeLabel.text = [NSString stringWithFormat:@"学者网公告: %@", dateTimeStr];
    
    [_titleLabel setTextContent:title];
    [XXTToolKit setView:_titleBgImageView toSizeH:_titleLabel.frame.size.height + TEXTINFO_MARGIN + 15];
    [XXTToolKit setView:_titleBgImageView toOriginY:_titleLabel.frame.origin.y - TEXTINFO_MARGIN * 2];
    
    [_hitLabel setTextContent:[NSString stringWithFormat:@"阅读热度%dC°", hit]];
    [XXTToolKit setView:_hitLabel toOriginX:DEVICE_SIZE.width - _hitLabel.frame.size.width - 35];
    [XXTToolKit setView:_hitLabel toOriginY:_titleLabel.frame.origin.y + _titleLabel.frame.size.height + TITLE_HIT_INTERVAL];
    [XXTToolKit setView:_hitBgImageView toSizeW:_hitLabel.frame.size.width + TEXTINFO_MARGIN * 4];
    [XXTToolKit setView:_hitBgImageView toOriginX:_hitLabel.frame.origin.x - TEXTINFO_MARGIN * 2];
    [XXTToolKit setView:_hitBgImageView toOriginY:_hitLabel.frame.origin.y - TEXTINFO_MARGIN * 2];
}

@end
