//
//  FeedBackViewController.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-3.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "FeedBackViewController.h"
#import "XXTHttpClient.h"
#import "BlocksKit.h"
#import <QuartzCore/QuartzCore.h>

@interface FeedBackViewController ()

@end

@implementation FeedBackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textView.placeholder = @"请输入内容(向下滑动收起键盘)";
    self.textView.delegate = self;
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.layer.borderColor = [UIColor blackColor].CGColor;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.cornerRadius = 5.0f;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleBordered target:self action:@selector(pressSendButton:)];
    rightItem.enabled = NO;
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationItem.title = @"发送反馈";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTextView:nil];
    [super viewDidUnload];
}

- (void)pressSendButton:(UIBarButtonItem *)barButton
{
    [KGStatusBar showWithStatus:@"正在发送反馈"];
    [self performBlock:^(id sender) {
        [KGStatusBar dismiss];
        [self performBlock:^(id sender) {
            [self requestToPostFeedBackWithContent:self.textView.text completeBlock:^{
                [KGStatusBar showSuccessWithStatus:@"发送反馈成功"];
            }];
        } afterDelay:0.5f];
    } afterDelay:1.0f];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 0) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

#pragma mark - URLRequest
- (void)requestToPostFeedBackWithContent:(NSString *)feedBack completeBlock:(VoidBlock)completeBlock
{
    NSString *url = [NSString stringWithFormat:@"%@?op=postfeedback", BASE_URL];
    NSDictionary *para = @{
                           kRequestName: [XXTToolKit getName],
                           kRequestMAuth: [XXTToolKit getMAuth],
                           kRequestContent: feedBack,
                           };
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:para successBlock:^(NSDictionary *responObject) {
        if (completeBlock) {
            completeBlock();
        }
    } errorBlock:^(NSString *str) {
        [KGStatusBar showErrorWithStatus:str];
    } failBlock:^{
        
    }];
}

@end
