//
//  XXTTextView.m
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTTextView.h"

@implementation XXTTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizesSubviews = YES;
        
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _bgImageView.image = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg.png"] withInsetTop:10 left:10 bottom:10 right:10];
        _bgImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self addSubview:_bgImageView];
        
        _textView = [[SSTextView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _textView.delegate = self;
        _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _textView.backgroundColor = [UIColor clearColor];
        [self addSubview:_textView];
    }
    return self;
}

- (void)setPlaceHolder:(NSString *)placeHolder
{
    _placeHolder = placeHolder;
    _textView.placeholder = placeHolder;
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    _textView.font = font;
}

- (BOOL)resignFirstResponder
{
    [_textView resignFirstResponder];
    return [super resignFirstResponder];
}

- (BOOL)becomeFirstResponder
{
    [_textView becomeFirstResponder];
    return [super becomeFirstResponder];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([_delegateInXXTTextView respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [_delegateInXXTTextView textViewDidBeginEditing:self];
    }
    
    self.bgImageView.image = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg_a.png"] withInsetTop:10 left:10 bottom:10 right:10];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_delegateInXXTTextView respondsToSelector:@selector(textViewDidEndEditing:)]) {
        [_delegateInXXTTextView textViewDidEndEditing:self];
    }
    
    self.bgImageView.image = [XXTToolKit getResizableImage:[UIImage imageNamed:@"edit_bg.png"] withInsetTop:10 left:10 bottom:10 right:10];
}

@end
