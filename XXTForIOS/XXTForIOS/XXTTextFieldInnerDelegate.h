//
//  XXTTextFieldInnerDelegate.h
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XXTTextFieldDelegate.h"

@interface XXTTextFieldInnerDelegate : NSObject <UITextFieldDelegate>

@property (strong, nonatomic) id <XXTTextFieldDelegate> delegateInXXTTextField;

@end
