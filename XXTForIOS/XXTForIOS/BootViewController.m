//
//  BootViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-16.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "BootViewController.h"
#import "SplitView.h"
#import "LoginViewController.h"
#import "BlocksKit.h"

@interface BootViewController () {
    BOOL shouldStopAnimation;
}

@end

@implementation BootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 5, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    
    self.pageControll = [[UIPageControl alloc] initWithFrame:CGRectMake(0, DEVICE_SIZE.height - 45, self.scrollView.frame.size.width, 30)];
    self.pageControll.numberOfPages = 5;
    self.pageControll.currentPage = 0;
    [self.view addSubview:self.pageControll];
    
    self.navigationController.navigationBarHidden = YES;
    
    [self beginAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setHandTouchImage:nil];
    [self setStartButton:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)pressStartButton:(id)sender {
    shouldStopAnimation = YES;
    
    SplitView *splitView = [[SplitView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height)];
    UIWindow *window = [XXTToolKit getWindow];
    splitView.alpha = 0;
    [window addSubview:splitView];
    
    [UIView animateWithDuration:0.3f animations:^{
        splitView.alpha = 1;
    } completion:^(BOOL finished) {
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        
        SBNavigationViewController *presentedNavCon = [[SBNavigationViewController alloc] init];
        [presentedNavCon pushViewController:loginViewController animated:NO];
        
        DISMISS_VIEWCONTROLLER(self, NO);
        PRESENT_VIEWCONTROLLER(presentedNavCon, [[XXTToolKit getAppdelegate] navigationController], NO);
        
        [self performBlock:^(id sender) {
            [splitView beginAnimation];
        } afterDelay:0.5f];
    }];
}

- (void)beginAnimation
{
    [self handAnimation];
    [self buttonAnimation];
}

- (void)handAnimation
{
    if (shouldStopAnimation) {
        return;
    }
    
    const CGFloat interval = 30;
    __block CGRect handRect = self.handTouchImage.frame;
    
    [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseInOut animations:^{
        handRect.origin.y += interval;
        self.handTouchImage.frame = handRect;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseInOut animations:^{
            handRect.origin.y -= interval;
            self.handTouchImage.frame = handRect;
        } completion:^(BOOL finished) {
            [self handAnimation];
        }];
    }];
}

- (void)buttonAnimation
{
    if (shouldStopAnimation) {
        return;
    }
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseInOut animations:^{
        self.startButton.alpha = 0.5f;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseInOut animations:^{
            self.startButton.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [self buttonAnimation];
        }];
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!((int)(scrollView.contentOffset.x) % 320)) {
        self.pageControll.currentPage = scrollView.contentOffset.x / self.scrollView.frame.size.width;
    }
}

@end
