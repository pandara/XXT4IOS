//
//  FriendFeedViewController.h
//  XXTForIOS
//
//  Created by pandara on 13-11-25.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendRelatedViewController.h"

@interface FriendFeedViewController : FriendRelatedViewController <UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) BOOL isMyFeedView;

@end
