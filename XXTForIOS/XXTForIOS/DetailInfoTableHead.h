//
//  DetailInfoTableHead.h
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailInfoTableHead : UIView

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *degreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

- (void)setDataWithShowedName:(NSString *)showedName
                       degree:(NSString *)degree
                       school:(NSString *)school
                        email:(NSString *)email
                 avatarURLStr:(NSString *)avatarURLStr;

@end
