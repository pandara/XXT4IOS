//
//  AppDelegate.h
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTabBarController.h"
#import "SBNavigationViewController.h"
#import "LoginViewController.h"
#import "LoadingView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UILocalNotification *_localNotificaiton;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SBTabBarController *tabBarController;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) SBNavigationViewController *navigationController;
@property (strong, nonatomic) NSMutableDictionary *appDict;
@property (strong, nonatomic) LoadingView *loadView;

@property (assign, nonatomic) BOOL isReLogin;//判断是否退出登录后的再次登录

- (SBTabBarController *)getMainTabBarController;
- (void)handleLocalNotificaiton:(UILocalNotification *)notification;

@end
