//
//  DetailInfoTableHead.m
//  XXTForIOS
//
//  Created by Pandara on 14-3-7.
//  Copyright (c) 2014年 pandara. All rights reserved.
//

#import "DetailInfoTableHead.h"
#import "UIImageView+JMImageCache.h"
#import <QuartzCore/QuartzCore.h>

@implementation DetailInfoTableHead

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"DetailInfoTableHead" owner:self options:nil] objectAtIndex:0];
    if (self) {
        // Initialization code
        [self initalize];
    }
    return self;
}

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"DetailInfoTableHead" owner:self options:nil] objectAtIndex:0];
    if (self) {
        [self initalize];
    }
    return self;
}

- (void)initalize
{
    self.avatarImageView.layer.borderColor = color(200, 200, 200, 1).CGColor;
    self.avatarImageView.layer.borderWidth = 1.0f;
    
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.schoolLabel.adjustsFontSizeToFitWidth = YES;
    self.emailLabel.adjustsFontSizeToFitWidth = YES;
    self.degreeLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)setDataWithShowedName:(NSString *)showedName
                       degree:(NSString *)degree
                       school:(NSString *)school
                        email:(NSString *)email
                 avatarURLStr:(NSString *)avatarURLStr
{
    [self.avatarImageView setImageWithURL:[NSURL URLWithString:avatarURLStr] placeholder:[UIImage imageNamed:@"avatar_default_rect.png"]];
    self.nameLabel.text = showedName;
    self.degreeLabel.text = degree;
    self.schoolLabel.text = school;
    self.emailLabel.text = email;
}



@end
