//
//  XXTTextField.h
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXTTextFieldInnerDelegate.h"
#import "XXTTextFieldDelegate.h"

@interface XXTTextField : UITextField {
    XXTTextFieldInnerDelegate *_innerDelegate;
}

@property (strong, nonatomic) id <XXTTextFieldDelegate> delegateInXXTTextField;

@end
