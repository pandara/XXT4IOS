//
//  FriendViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-9-12.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "FriendViewController.h"
#import "UIButton+Block.h"
#import "XXTTableCell.h"
#import "XXTHttpClient.h"
#import "ChatingDetailViewController.h"

@interface FriendViewController ()

@property (strong, nonatomic) UIButton *searchMaskButton;
@property (strong, nonatomic) NSArray *testTitleArray;
@property (strong, nonatomic) NSArray *testSubTitleArray;
@end

@implementation FriendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.friendArray = [[NSMutableArray alloc] init];
    [self.friendArray addObjectsFromArray:[XXTToolKit getDataFromFriendCache]];
    
    self.searchResultArray = [[NSMutableArray alloc] init];
    [self.searchResultArray addObjectsFromArray:self.friendArray];
    
    //searchMaskButton;
    _searchMaskButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    _searchMaskButton.backgroundColor = color(0, 0, 0, 0.5);
    [_searchMaskButton handleControlEvent:UIControlEventTouchUpInside withBlock:^{
        [self.searchBar resignFirstResponder];
    }];
    
    //tableView
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = YES;
    
    //设置searchBar
    self.searchBar.placeholder = @"搜索";
    self.searchBar.delegate = self;
    
    UIView *searchBarBgView = [[self.searchBar subviews] objectAtIndex:0];
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, searchBarBgView.frame.size.width, searchBarBgView.frame.size.height)];
    bgImageView.image = [UIImage imageNamed:@"searchbar_bg.png"];
    [searchBarBgView addSubview:bgImageView];
    
    //test data
    _testTitleArray = @[@"吴大狗", @"Willard Smith", @"文烧饼"];
    _testSubTitleArray = @[@"小学鸡", @"硕士研究生", @"无法解析"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:NOTIFI_REFRESH_ALL_CON object:nil];
}

- (void)refresh
{
    [self.friendArray removeAllObjects];
    [self.tableView reloadData];
    [self requestFriendList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [APP_NAVIGATIONBAR_FIRSTITEM setTitle:@"好友"];
    [self requestFriendList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
}

- (void)refreshSearchResult
{
    NSString *searchName = self.searchBar.text;
    [self.searchResultArray removeAllObjects];
    
    if (searchName.length == 0) {
        [self.searchResultArray addObjectsFromArray:self.friendArray];
        [self.tableView reloadData];
        return;
    }
    
    for (NSDictionary *friendInfo in self.friendArray) {
        NSString *showName = [XXTToolKit getNameFromNick:emptyStr([friendInfo objectForKey:kRequestNick]) andName:emptyStr([friendInfo objectForKey:kRequestName])];
        
        if ([showName rangeOfString:searchName].location != NSNotFound) {
            [self.searchResultArray addObject:friendInfo];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResultArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = [self.searchResultArray objectAtIndex:indexPath.row];
    XXTTableCell *cell = [tableView dequeueReusableCellWithIdentifier:XXTTABLECELL_ID];
    if (cell == nil) {
        cell = [[XXTTableCell alloc] init];
        [cell setStyle:XXTTableCellStyleNoTime];
    }
    
    NSString *showName = [XXTToolKit getNameFromNick:[dataDict objectForKey:kRequestNick] andName:[dataDict objectForKey:kRequestName]];
    
    [cell setImageURLStr:emptyStr([dataDict objectForKey:kRequestAvatarURL])
                   title:showName
                subTitle:emptyStr([dataDict objectForKey:kRequestDegree])];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *friendInfoDict = [self.friendArray objectAtIndex:indexPath.row];
    NSString *toUserName = [friendInfoDict objectForKey:kRequestName];
    NSString *toUserNick = [friendInfoDict objectForKey:kRequestNick];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    ChatingDetailViewController *chatDetailViewController = [[ChatingDetailViewController alloc] initWithNibName:@"ChatingDetailViewController" bundle:nil withToUserName:toUserName andToUserNick:toUserNick];
    
    [self.navigationController pushViewController:chatDetailViewController animated:YES];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (![_searchMaskButton superview]) {
        _searchMaskButton.alpha = 0;
        [self.view addSubview:_searchMaskButton];
        
        [UIView animateWithDuration:0.2f animations:^{
            _searchMaskButton.alpha = 1;
        }];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refreshSearchResult];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([_searchMaskButton superview]) {
        [UIView animateWithDuration:0.2f animations:^{
            _searchMaskButton.alpha = 0;
        } completion:^(BOOL finished) {
            [_searchMaskButton removeFromSuperview];
        }];
    }
}

#pragma mark - URLRequest
- (void)requestFriendList
{
    NSString *url = [NSString stringWithFormat:@"%@?op=friendlist&mauth=%@&name=%@", BASE_URL, [XXTToolKit getMAuth], [XXTToolKit getName]];
    [[XXTHttpClient sharedInstance] requestWithURLStr:url para:nil successBlock:^(id responObject) {
        responObject = (NSArray *)responObject;
        [self.friendArray removeAllObjects];
        [self.friendArray addObjectsFromArray:[responObject objectForKey:kRequestData]];
        [self refreshSearchResult];
        [XXTToolKit setDataToFriendCache:self.friendArray];
        
    } failBlock:^{
        
    }];
}

@end
