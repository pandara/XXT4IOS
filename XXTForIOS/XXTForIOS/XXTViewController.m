//
//  XXTViewController.m
//  XXTForIOS
//
//  Created by pandara on 13-11-26.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTViewController.h"
#import "FlowLayoutLabel.h"
#import "BlocksKit.h"

@interface XXTViewController ()

@end

@implementation XXTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _juhua = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self doneLoading];
}

- (void)startLoading
{
    [self performBlock:^(id sender) {
        CGFloat titleW = [FlowLayoutLabel getHeightFromText:self.navigationItem.title maxWidth:320 maxLine:1 font:[UIFont systemFontOfSize:20] fontSize:20].width;
        [XXTToolKit setView:_juhua toOriginX:(DEVICE_SIZE.width - titleW) / 2 - 20 - 10];
        [XXTToolKit setView:_juhua toOriginY:(44 - 20) / 2];
        [self.navigationController.navigationBar addSubview:_juhua];
        [_juhua startAnimating];
    } afterDelay:0.3f];
}

- (void)doneLoading
{
    [self performBlock:^(id sender) {
        [_juhua removeFromSuperview];
        [_juhua stopAnimating];
    } afterDelay:0.3f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData
{
    
}

@end
