//
//  XXTHttpClient.m
//  XXTForIOS
//
//  Created by pandara on 13-9-18.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTHttpClient.h"
#import "SVProgressHUD.h"

@implementation XXTHttpClient

+ (XXTHttpClient *)sharedInstance
{
    static XXTHttpClient *shareClient;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareClient = [[XXTHttpClient alloc] init];
        shareClient.operationArray = [[NSMutableArray alloc] init];
    });
    
    return shareClient;
}

- (void)requestWithURLStr:(NSString *)urlStr para:(NSDictionary *)para successBlock:(XXTHttpSuccessBlock)successBlock failBlock:(XXTHttpFailBlock)failBlock
{
    [self requestWithURLStr:urlStr para:para successBlock:successBlock errorBlock:nil failBlock:failBlock];
}

- (void)requestWithURLStr:(NSString *)urlStr para:(NSDictionary *)para successBlock:(XXTHttpSuccessBlock)successBlock errorBlock:(XXTHttpErrorBlock)errorBlock failBlock:(XXTHttpFailBlock)failBlock
{
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@""]];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlStr parameters:para];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [self.operationArray addObject:operation];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
//        NSString *str = [[NSString alloc] initWithData:(NSData *)responseObject encoding:enc];
//        NSData *utf8Data = [str dataUsingEncoding:NSUTF8StringEncoding];
//        id responObject = [utf8Data objectFromJSONData];
        responseObject = [responseObject objectFromJSONData];
        
        [self.operationArray removeObject:operation];

        if ([[responseObject objectForKey:kRequestError] intValue] == 1) {
            XXTLog(@"接口请求错误:%@", [responseObject objectForKey:kRequestMsg]);
            if (errorBlock) {
                errorBlock([responseObject objectForKey:kRequestMsg]);
            } else {
                [SVProgressHUD showErrorWithStatus:[responseObject objectForKey:kRequestMsg]];
            }
            return;
        }
        
        if (successBlock) {
            successBlock((NSDictionary *)responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.operationArray removeObject:operation];
        [SVProgressHUD showErrorWithStatus:@"网络异常"];
        XXTLog(@"网络异常error:%@", error);
        if (failBlock) {
            failBlock();
        }
    }];
    [operation start];
}

- (void)cancelAllOperation
{
    for (AFHTTPRequestOperation *operation in self.operationArray) {
        [operation cancel];
    }
}

@end
