//
//  BubbleLoadMoreHead.m
//  familyhd
//
//  Created by pandara on 13-12-5.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "BubbleLoadMoreHead.h"
#import <QuartzCore/QuartzCore.h>

#define HEIGHT 50
#define ANIMATE_DUR 0.3f

@implementation BubbleLoadMoreHead

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"BubbleLoadMoreHead" owner:self options:nil] objectAtIndex:0];
    if (self) {
        [self.juhua startAnimating];
        self.bgView.layer.cornerRadius = 5.0f;
    }
    return self;
}

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"BubbleLoadMoreHead" owner:self options:nil] objectAtIndex:0];
    if (self) {
        [self.juhua startAnimating];
        self.bgView.layer.cornerRadius = 5.0f;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    UIView *superView = self.superview;
    while (![superView isKindOfClass:[UIScrollView class]] && superView) {
        superView = superView.superview;
    }
    if (superView != nil) {
        CGRect frame = superView.frame;
        self.center = CGPointMake(frame.size.width / 2, -HEIGHT / 2);
    }
    
    [super drawRect:rect];
}

#warning 这里设置contentOffset的过程会引起scrollViewDidScroll这个delegate被调用，如果改变加载更多的时机，bubbleTableView的setToLoadMore要修改
- (void)setToLoadMore
{
    UIView *superView = self.superview;
    if (![superView isKindOfClass:[UIScrollView class]] && superView) {
        superView = superView.superview;
    }
    
    if (superView) {
        UIScrollView *scrollView = (UIScrollView *)superView;
        
//        scrollView.contentOffset = CGPointMake(-HEIGHT, 0); 
        scrollView.contentInset = UIEdgeInsetsMake(HEIGHT, 0, 0, 0);
    }
}

- (void)endLoadMore
{
    _isLoading = NO;
    
    UIView *superView = self.superview;
    if (![superView isKindOfClass:[UIScrollView class]] && superView) {
        superView = superView.superview;
    }
    
    if (superView) {
        UIScrollView *scrollView = (UIScrollView *)superView;
        
        CGFloat newContentSizeH = scrollView.contentSize.height;
        if (newContentSizeH == _originalContentSizeH) {// && _originalContentSizeH != 0) {
            scrollView.contentOffset = CGPointMake(0, -HEIGHT);
            [UIView animateWithDuration:ANIMATE_DUR animations:^{
                scrollView.contentOffset = CGPointMake(0, 0);
                scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            }];
        } else if (scrollView.contentInset.top != 0) {
            [UIView animateWithDuration:ANIMATE_DUR animations:^{
                scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            }];
        }
//        if (scrollView.contentInset.top != 0) {
//            scrollView.contentOffset = CGPointMake(0, -HEIGHT);
//            [UIView animateWithDuration:ANIMATE_DUR animations:^{
//                scrollView.contentOffset = CGPointMake(0, 0);
//                scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//            }];
//        }
    }
}

- (void)scheduleLoadMore:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y <= -HEIGHT) {
        _isLoading = YES;
        
        _originalContentSizeH = scrollView.contentSize.height;
        
        if ([_delegate respondsToSelector:@selector(loadMoreHeadDidStartToLoad:)]) {
            [_delegate loadMoreHeadDidStartToLoad:self];
        }
//        [UIView animateWithDuration:ANIMATE_DUR animations:^{
            scrollView.contentInset = UIEdgeInsetsMake(HEIGHT, 0, 0, 0);
//        }];
    }
}

- (void)scrollViewDidEndDecelerate:(UIScrollView *)scrollView
{
    [self scheduleLoadMore:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self scheduleLoadMore:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= 0) {
        return;
    }
    
    CGFloat alpha = MIN(-scrollView.contentOffset.y, HEIGHT) / HEIGHT;
    self.alpha = alpha;
    
    if (scrollView.contentOffset.y <= -HEIGHT && scrollView.contentInset.top == 0) {//当decelerating令contentOffset超过height也能加载更多
        [UIView animateWithDuration:ANIMATE_DUR animations:^{
            scrollView.contentInset = UIEdgeInsetsMake(HEIGHT, 0, 0, 0);
            scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y + HEIGHT);
        }];
    }
}

@end
