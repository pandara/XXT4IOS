//
//  XXTTextView.h
//  XXTForIOS
//
//  Created by pandara on 13-11-28.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSTextView.h"

@class XXTTextView;
@protocol XXTTextViewDelegate <NSObject>

- (void)textViewDidBeginEditing:(XXTTextView *)textView;
- (void)textViewDidEndEditing:(XXTTextView *)textView;

@end

@interface XXTTextView : UIView <UITextViewDelegate>

@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) NSString *placeHolder;
@property (strong, nonatomic) SSTextView *textView;
@property (strong, nonatomic) UIImageView *bgImageView;
@property (strong, nonatomic) id <XXTTextViewDelegate> delegateInXXTTextView;


@end
