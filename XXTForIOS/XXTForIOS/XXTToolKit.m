//
//  XXTToolKit.m
//  XXTForIOS
//
//  Created by pandara on 13-9-13.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import "XXTToolKit.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "SSKeychain.h"
#import "XXTHttpClient.h"

@implementation XXTToolKit

+ (UIImage *)getImageFromColor:(UIColor *)color withRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (BOOL)isNotFirstOpenApp
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isfirstopenapp"];
}

+ (void)setNotFirstOpenApp:(BOOL)firstOpenApp
{
    return [[NSUserDefaults standardUserDefaults] setBool:firstOpenApp forKey:@"isfirstopenapp"];
}

+ (AppDelegate *)getAppdelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (UIWindow *)getWindow
{
    return [[XXTToolKit getAppdelegate] window];
}

//+ (NSString *)getMD5EncodeFromString:(NSString *)string
//{
//    const char *original_str = [string UTF8String];
//    unsigned char result[CC_MD5_DIGEST_LENGTH];
//    CC_MD5(original_str, strlen(original_str), result);
//    NSMutableString *hash = [NSMutableString string];
//    for (int i = 0; i < 16; i++) {
//        [hash appendFormat:@"%02X", result[i]];
//    }
//    return [hash lowercaseString];
//}
//
////byte 数组转换为16进制的字符串
//- (NSString *)byteArr2HexStr:(Byte *)byteArr len:(int)len
//{
//    NSMutableString *string = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < len; i++) {
//        int intTmp = byteArr[i];
//        
//        //把负数转换为正数
//        while (intTmp < 0) {
//            intTmp += 256;
//        }
//        
//        [string appendFormat:@"%02X", intTmp];
//    }
//    
//    return string;
//}
//
////static Byte iv[] = {1,2,3,4,5,6,7,8}; 
//
//+ (NSString *)encryptUseDES:(NSString *)clearText key:(NSString *)key
//{
//    NSString *ciphertext = nil;
//    NSData *textData = [clearText dataUsingEncoding:NSUTF8StringEncoding];
//    NSUInteger dataLength = [clearText length];
////    NSLog(@"clearText:%@", clearText);
////    NSLog(@"dataLength:%d", dataLength);
//    unsigned char buffer[2048];
//    memset(buffer, 0, sizeof(char));
//    size_t numBytesEncrypted = 0;
//
//
//    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES, kCCOptionPKCS7Padding|kCCOptionECBMode, [key UTF8String], kCCKeySizeDES, NULL, [textData bytes], dataLength, buffer, 2048, &numBytesEncrypted);
////    NSLog(@"numberBytesEncrypted:%d", (int)(numBytesEncrypted));
//    if (cryptStatus == kCCSuccess) {
//
//        NSData *data = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesEncrypted];
//        Byte* bb = (Byte*)[data bytes]; 
//        ciphertext = [XXTToolKit parseByteArray2HexString:bb length:numBytesEncrypted];
//    }else{
//        NSLog(@"DES加密失败");
//    }
//    return ciphertext; 
//}
//
//+ (NSString *)parseByteArray2HexString:(Byte[]) bytes length:(int)length
//{
//    NSMutableString *hexStr = [[NSMutableString alloc]init];
//
//    if(bytes) {
//        for (int i = 0; i < length; i++) {
//            
//            int intTmp = bytes[i];
//            
//            NSString *hexByte = [NSString stringWithFormat:@"%x", intTmp & 0xff];///16进制数
//            if([hexByte length]==1)
//                [hexStr appendFormat:@"0%@", hexByte];
//            else
//                [hexStr appendFormat:@"%@", hexByte];
//        }
//    }
////    NSLog(@"bytes 的16进制数为:%@",hexStr); 
//    return [hexStr lowercaseString];
//}
//
//+ (NSString *)paresString2HexString:(NSString *)originString
//{
//    NSData *stringData = [originString dataUsingEncoding:NSUTF8StringEncoding];
//    Byte *bytes = (Byte *)[stringData bytes];
//    
//    NSMutableString *hexString = [[NSMutableString alloc] init];
//    for (int i = 0; i < [stringData length]; i++) {
//        [hexString appendFormat:@"%02X", bytes[i] & 0xff];
//    }
//
//    return [hexString uppercaseString];
//}

//auth
+ (void)setMAuth:(NSString *)mauth
{
//    [SSKeychain setPassword:auth forService:kKeyChainServiceAuth account:[XXTToolKit getAccount]];
    [SSKeychain setPassword:mauth forService:kKeyChainServiceAuth account:[XXTToolKit getName]];
}

+ (NSString *)getMAuth
{
//    return [SSKeychain passwordForService:kKeyChainServiceAuth account:[XXTToolKit getAccount]];
    return [SSKeychain passwordForService:kKeyChainServiceAuth account:[XXTToolKit getName]];
}

//passwd
+ (void)setPasswd:(NSString *)passwd
{
//    [SSKeychain setPassword:passwd forService:kKeyChainServicePasswd account:[XXTToolKit getAccount]];
    [SSKeychain setPassword:passwd forService:kKeyChainServicePasswd account:[XXTToolKit getName]];
}

+ (NSString *)getPasswd
{
//    return [SSKeychain passwordForService:kKeyChainServicePasswd account:[XXTToolKit getAccount]];
    return [SSKeychain passwordForService:kKeyChainServicePasswd account:[XXTToolKit getName]];
}

//account
//+ (void)setAccount:(NSString *)account
//{
//    [[NSUserDefaults standardUserDefaults] setObject:account forKey:kUDAccount];
//}

//+ (NSString *)getAccount
//{
//    return [[NSUserDefaults standardUserDefaults] objectForKey:kUDAccount];
//}

//clientID
//+ (NSString *)getClientID
//{
//    return [[XXTToolKit getUserInfoDict] objectForKey:kInfoDictClientID];
//}

//+ (NSString *)getACCID
//{
//    return [[XXTToolKit getUserInfoDict] objectForKey:kInfoDictAccid];
//}

//infoDict
//+ (void)setUserInfoDictwithID:(NSString *)accid
//                     userName:(NSString *)userName
//                 avatarURLStr:(NSString *)avatartURLStr
//                  chnUserName:(NSString *)chnUserName
//                        title:(NSString *)title
//                         time:(NSString *)time
//{
//    NSString *clientID = [[[NSString stringWithFormat:@"%llX", [time longLongValue]] substringFromIndex:2] uppercaseString];
//    
//    NSDictionary *infoDict = @{kInfoDictAccid : accid,
//                               kInfoDictCHNUserName : userName,
//                               kInfoDictAvatarURL : avatartURLStr,
//                               kInfoDictCHNUserName : chnUserName,
//                               kInfoDictTitle : title,
//                               kInfoDictTime : time,
//                               kInfoDictClientID : clientID};
//    
//    [[NSUserDefaults standardUserDefaults] setObject:infoDict forKey:kUDInfoDict];
//}

//+ (NSDictionary *)getUserInfoDict
//{
//    return [[NSUserDefaults standardUserDefaults] objectForKey:kUDInfoDict];
//}

//+ (NSString *)getAvatarURLStr:(NSString *)tempAvatarURLStr
//{
//    //http://www.scholat.com/resources/fucz/person/profile/1379479454458.jpg
//    //fucz@1379479454458.jpg
//    NSRange atRange = [tempAvatarURLStr rangeOfString:@"@"];
//    if (atRange.location == NSNotFound) {
//        return @"";
//    }
//    
//    NSString *name = [tempAvatarURLStr substringWithRange:NSMakeRange(0, atRange.location)];
//    NSString *picID = [tempAvatarURLStr substringWithRange:NSMakeRange(atRange.location + 1, tempAvatarURLStr.length - (atRange.location + 1))];
//    
//    return [NSString stringWithFormat:@"http://www.scholat.com/resources/%@/person/profile/%@", name, picID];
//}

//朋友列表缓存
+ (void)setDataToFriendCache:(NSArray *)friendList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *filename=[path stringByAppendingPathComponent:kFileFriendListCache];
    [friendList writeToFile:filename atomically:YES];
}

+ (void)deleteDataFromFriendCache
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileFriendListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filename]) {
        [fileManager removeItemAtPath:filename error:nil];
    }
}

+ (NSArray *)getDataFromFriendCache
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileFriendListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filename]) {
        return nil;
    }
    
    NSArray *cacheList = [[NSArray alloc] initWithContentsOfFile:filename];
    return cacheList;
}

//聊天记录缓存
+ (void)setDataToChatListCache:(NSArray *)chatList fromUserInfo:(NSDictionary *)fromUserDict toUserInfo:(NSDictionary *)toUserDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *filename=[path stringByAppendingPathComponent:kFileChatListCache];
    
    NSMutableDictionary *chatListNameDict = [[NSMutableDictionary alloc] init];
    [chatListNameDict addEntriesFromDictionary:[XXTToolKit getAllDataFromChatListCache]];
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:chatList forKey:kRequestChatList];
    [dataDict setObject:fromUserDict forKey:kRequestFromUser];
    [dataDict setObject:toUserDict forKey:kRequestToUser];
    
    [chatListNameDict setObject:dataDict forKey:[toUserDict objectForKey:kRequestName]];
    [chatListNameDict writeToFile:filename atomically:YES];
}

+ (CGFloat)getSizeOfChatListCache//单位为B
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *filename=[path stringByAppendingPathComponent:kFileChatListCache];
    
    NSError *error = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filename error:&error];
    if (error) {
        XXTLog(@"获取聊天缓存属性错误：%@", error);
        return 0;
    }
    
    return [fileAttributes fileSize];
}

+ (void)deleteDataFromChatListCacheWithErrorBlock:(StringBlock)errorBlock completeBlock:(VoidBlock)completeBlock
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileChatListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:filename]) {
        [fileManager removeItemAtPath:filename error:&error];
    }
    
    if (error) {
        if (errorBlock) {
            errorBlock([error localizedDescription]);
        }
    } else {
        if (completeBlock) {
            completeBlock();
        }
    }
}

+ (NSDictionary *)getDataFromChatListCacheWithName:(NSString *)name
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileChatListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filename]) {
        return nil;
    }
    
    NSDictionary *chatList_NameDict = [[NSDictionary alloc] initWithContentsOfFile:filename];
    return [chatList_NameDict objectForKey:name];
}

+ (NSDictionary *)getAllDataFromChatListCache
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileChatListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filename]) {
        return nil;
    }
    
    NSDictionary *chatListNameDict = [[NSDictionary alloc] initWithContentsOfFile:filename];
    return chatListNameDict;
}

//最近谈话列表缓存
+ (void)setDataToAllChatListCache:(NSArray *)chatList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *filename=[path stringByAppendingPathComponent:kFileAllChatListCache];
    
    [chatList writeToFile:filename atomically:YES];
}

+ (NSArray *)getDataFromAllChatListCache
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileAllChatListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filename]) {
        return nil;
    }
    
    NSArray *allChatList = [[NSArray alloc] initWithContentsOfFile:filename];
    return allChatList;
}

+ (void)deleteDataFromAllChatListCache
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:kFileAllChatListCache];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filename]) {
        [fileManager removeItemAtPath:filename error:nil];
    }

}

//+ (NSString *)getAuthFromAccount:(NSString *)account passwd:(NSString *)passwd
//{
//    NSString *epsw = passwd;
//    
//    NSDate *date = [NSDate date];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"MMdd"];
//    NSString *dateStr = [formatter stringFromDate:date];
//    
//    NSString *authOriginal = [NSString stringWithFormat:@"%@%@%@%@%@", account, @"<xxtauth>", dateStr, @"</xxtauth>", epsw];
//    NSString *auth = [XXTToolKit encryptUseDES:authOriginal key:DEFAULT_KEY];
//    return auth;
//}

+ (void)setView:(UIView *)view toOriginX:(CGFloat)x
{
    CGRect frame = view.frame;
    frame.origin.x = x;
    view.frame = frame;
}

+ (void)setView:(UIView *)view toSizeH:(CGFloat)h
{
    CGRect frame = view.frame;
    frame.size.height = h;
    view.frame = frame;
}

+ (void)setView:(UIView *)view toSizeW:(CGFloat)w
{
    CGRect frame = view.frame;
    frame.size.width = w;
    view.frame = frame;
}

+ (void)setView:(UIView *)view toOriginY:(CGFloat)y
{
    CGRect frame = view.frame;
    frame.origin.y = y;
    view.frame = frame;
}

+ (void)setFriendInfoList:(NSArray *)friendInfoList
{
    [[NSUserDefaults standardUserDefaults] setObject:friendInfoList forKey:kUserDefaultFriendList];
}

+ (NSArray *)getFriendInfoList
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultFriendList];
}

//+ (NSDictionary *)getFriendInfoDictFromID:(NSString *)fid
//{
//    NSArray *friendArray = [XXTToolKit getFriendInfoList];
//
//    for (NSDictionary *dataDict in friendArray) {
//        if ([[dataDict objectForKey:kRequestID] isEqualToString:fid]) {
//            return dataDict;
//        }
//    }
//    
//    return @{};
//}

+ (NSString *)getTimeStrFromDate:(NSDate *)date withFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

+ (NSString *)getFormatTimeStrFromDate:(NSDate *)date withFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [XXTToolKit getFormatTimeStrFromTimeStr:[formatter stringFromDate:date]];
}

+ (NSString *)getFormatTimeStrFromDate:(NSDate *)date
{
    return [XXTToolKit getFormatTimeStrFromDate:date withFormat:@"yyyy-MM-dd HH:mm"];
}

+ (NSString *)getFormatTimeStrFromTimeStr:(NSString *)timeStr
{
    NSRange range = [timeStr rangeOfString:@"-"];
    if (range.location == NSNotFound) {
        return timeStr;
    }
    
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *year = [formatter stringFromDate:now];
    
    NSString *timeStrYear = [timeStr substringWithRange:NSMakeRange(0, range.location)];
    
    if ([timeStrYear isEqualToString:year]) {
        return [timeStr substringFromIndex:range.location + 1];
    }
    
    return timeStr;
}

+ (void)setFriendFeedList:(NSArray *)friendList
{
    [[NSUserDefaults standardUserDefaults] setObject:friendList forKey:kUserDefaultFriendFeedList];
}

+ (NSArray *)getFriendFeedList
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultFriendFeedList];
}

+ (void)setMyFeedList:(NSArray *)feedList
{
    [[NSUserDefaults standardUserDefaults] setObject:feedList forKey:kUserDefaultMyFeedList];
}

+ (NSArray *)getMyFeedList
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultMyFeedList];
}

+ (UIImage *)getResizableImage:(UIImage *)image
                  withInsetTop:(CGFloat)top
                          left:(CGFloat)left
                        bottom:(CGFloat)bottom
                         right:(CGFloat)right
{
    if (DEVICE_OS_VERSION < 6.0f) {
        return [image stretchableImageWithLeftCapWidth:right topCapHeight:top];
    }
    
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(top, left, bottom, right) resizingMode:UIImageResizingModeStretch];
}

+ (void)setView:(UIView *)view toOrigin:(CGPoint)origin
{
    CGRect frame = view.frame;
    frame.origin = origin;
    view.frame = frame;
}

+ (NSTimeInterval)getCurrentTimeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    return [date timeIntervalSince1970];
}

+ (void)setCalendarArray:(NSArray *)calendarArray
{
    [[NSUserDefaults standardUserDefaults] setObject:calendarArray forKey:kUserDefaultCalendarList];
}

+ (NSArray *)getCalendarArray
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCalendarList];
}

//php------------------------------------------------------------------
+ (void)saveUserData:(NSDictionary *)loginDataDict
{
    [[NSUserDefaults standardUserDefaults] setObject:loginDataDict forKey:kUserDefaultUserInfoDict];
}

+ (NSDictionary *)getUserInfoDict
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultUserInfoDict];
}

+ (NSString *)getName
{
    return [[XXTToolKit getUserInfoDict] objectForKey:kInfoDictName];
}

+ (void)setName:(NSString *)name
{
    NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init];
    [userInfoDict addEntriesFromDictionary:[XXTToolKit getUserInfoDict]];
    
    [userInfoDict setObject:name forKey:kInfoDictName];
    
    [XXTToolKit saveUserData:userInfoDict];
}

+ (NSString *)getAvatarURLStr
{
    return emptyStr([[self getUserInfoDict] objectForKey:kRequestAvatarURL]);
}

//获取需要显示的格式化日期串：同年不显示年份
+ (NSString *)getFormatDateStrFromDateLine:(NSString *)dateLine
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    
    NSDate *nowDate = [NSDate dateWithTimeIntervalSinceNow:0];
    int nowYear = [[formatter stringFromDate:nowDate] intValue];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dateLine doubleValue]];
    int year = [[formatter stringFromDate:date] intValue];
    
    if (nowYear == year) {//同年
        [formatter setDateFormat:@"MM-dd HH:mm"];
    } else {//不同年
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    }
    
    return [formatter stringFromDate:date];
}

+ (NSString *)getNameFromNick:(NSString *)nick andName:(NSString *)name
{
    if ((NSNull *)nick == [NSNull null] || nick.length == 0 || !nick) {
        return name;
    }
    return nick;
}

+ (void)setHasLogined:(BOOL)hasLogined
{
    [[NSUserDefaults standardUserDefaults] setBool:hasLogined forKey:kUserDefaultHasLogined];
}

+ (BOOL)getHasLogined
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultHasLogined];
}

@end







