//
//  XXTToolKit.h
//  XXTForIOS
//
//  Created by pandara on 13-9-13.
//  Copyright (c) 2013年 pandara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface XXTToolKit : NSObject

+ (UIImage *)getImageFromColor:(UIColor *)color withRect:(CGRect)rect;

+ (BOOL)isNotFirstOpenApp;
+ (void)setNotFirstOpenApp:(BOOL)firstOpenApp;

+ (UIWindow *)getWindow;
+ (AppDelegate *)getAppdelegate;

//+ (NSString *)getMD5EncodeFromString:(NSString *)string;
//+(NSString *) encryptUseDES:(NSString *)clearText key:(NSString *)key;

//+ (NSString *)getAccount;
//+ (void)setAccount:(NSString *)account;

+ (void)setPasswd:(NSString *)passwd;
+ (NSString *)getPasswd;

+ (void)setMAuth:(NSString *)mauth;
+ (NSString *)getMAuth;

//+ (void)setUserInfoDictwithID:(NSString *)accid
//                     userName:(NSString *)userName
//                 avatarURLStr:(NSString *)avatartURLStr
//                  chnUserName:(NSString *)chnUserName
//                        title:(NSString *)title
//                         time:(NSString *)time;

//+ (NSString *)getClientID;
//+ (NSString *)getACCID;

//+ (NSString *)getAvatarURLStr:(NSString *)tempAvatarURLStr;

+ (void)setDataToFriendCache:(NSArray *)friendList;
+ (void)deleteDataFromFriendCache;
+ (NSArray *)getDataFromFriendCache;

//+ (NSString *)getAuthFromAccount:(NSString *)account passwd:(NSString *)passwd;

+ (void)setView:(UIView *)view toOriginX:(CGFloat)x;
+ (void)setView:(UIView *)view toSizeH:(CGFloat)h;
+ (void)setView:(UIView *)view toSizeW:(CGFloat)w;
+ (void)setView:(UIView *)view toOriginY:(CGFloat)y;
+ (void)setView:(UIView *)view toOrigin:(CGPoint)origin;

+ (void)setFriendInfoList:(NSArray *)frendInfoList;
+ (NSArray *)getFriendInfoList;
+ (NSDictionary *)getFriendInfoDictFromID:(NSString *)fid;

+ (NSString *)getFormatTimeStrFromTimeStr:(NSString *)timeStr;
+ (NSArray *)getFriendFeedList;
+ (void)setFriendFeedList:(NSArray *)friendList;
+ (UIImage *)getResizableImage:(UIImage *)image
                  withInsetTop:(CGFloat)top
                          left:(CGFloat)left
                        bottom:(CGFloat)bottom
                         right:(CGFloat)right;

+ (NSString *)getFormatTimeStrFromDate:(NSDate *)date;
+ (NSString *)getFormatTimeStrFromDate:(NSDate *)date withFormat:(NSString *)format;
+ (NSTimeInterval)getCurrentTimeInterval;
+ (NSString *)getTimeStrFromDate:(NSDate *)date withFormat:(NSString *)format;

+ (NSArray *)getCalendarArray;
+ (void)setCalendarArray:(NSArray *)calendarArray;

//php--------------------------------------
+ (void)saveUserData:(NSDictionary *)loginDataDict;
+ (NSDictionary *)getUserInfoDict;
+ (NSString *)getName;
+ (void)setName:(NSString *)name;
+ (NSString *)getAvatarURLStr;
+ (NSString *)getFormatDateStrFromDateLine:(NSString *)dateLine;
+ (NSString *)getNameFromNick:(NSString *)nick andName:(NSString *)name;
+ (void)setHasLogined:(BOOL)hasLogined;
+ (BOOL)getHasLogined;
+ (NSArray *)getMyFeedList;
+ (void)setMyFeedList:(NSArray *)feedList;

+ (void)setDataToChatListCache:(NSArray *)chatList fromUserInfo:(NSDictionary *)fromUserDict toUserInfo:(NSDictionary *)toUserDict;
+ (void)deleteDataFromChatListCacheWithErrorBlock:(StringBlock)errorBlock completeBlock:(VoidBlock)completeBlock;
+ (NSDictionary *)getDataFromChatListCacheWithName:(NSString *)name;
+ (NSDictionary *)getAllDataFromChatListCache;
+ (CGFloat)getSizeOfChatListCache;

+ (void)deleteDataFromAllChatListCache;
+ (NSArray *)getDataFromAllChatListCache;
+ (void)setDataToAllChatListCache:(NSArray *)chatList;
@end









