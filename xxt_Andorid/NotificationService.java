﻿/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scholat.xxt;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.scholat.xxt.db.CalendarHelper;
import com.scholat.xxt.db.DynamicHelper;
import com.scholat.xxt.db.FriendHelper;
import com.scholat.xxt.db.MsgHelper;
import com.scholat.xxt.po.Alert;
import com.scholat.xxt.NotificationService;
import com.scholat.xxt.xmpp.XmppManager;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

/**
 * 消息服务
 * 
 * @author fuchzhou@gmail.com
 */
public class NotificationService extends Service {

	private static final String LOGTAG = NotificationService.class.getSimpleName();
	public static final String SERVICE_NAME = "com.scholat.xxt.NotificationService";

	private ExecutorService executorService;
	private TaskSubmitter taskSubmitter;
	private TaskTracker taskTracker;
	public XmppManager xmppManager;
	public static NotificationService INSTANCE;
	public static Context CONTEXT;

	public NotificationService() {

		executorService = Executors.newSingleThreadExecutor();
		taskSubmitter = new TaskSubmitter(this);
		taskTracker = new TaskTracker(this);

	}

	@Override
	public void onCreate() {

		// 定时检测程序已失效
		final Timer appTimer = new Timer();
		appTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (BackgroundService.isMainActivityRunning == false) {
					Log.d(LOGTAG, "App stop()...");
					Intent intent = NotificationService.getIntent();
					NotificationService.this.stopService(intent);
					try {
						appTimer.cancel();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}, BackgroundService.APP_TIMER_INTERVAL, BackgroundService.APP_TIMER_INTERVAL);

		xmppManager = new XmppManager(NotificationService.this);
		INSTANCE = this;
		CONTEXT = this;
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				taskSubmitter.submit(new Runnable() {
					@Override
					public void run() {
						Log.d(LOGTAG, "NotificationService starting...");
						xmppManager.connect();
					}
				});
			}
		}, 3000);

		alertHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (!BackgroundService.isAlerting) {
					Alert alert = (Alert) msg.obj;
					Intent intent = new Intent(INSTANCE, AlertActivity.class);
					intent.putExtra("event", alert.getCont());
					intent.putExtra("date", alert.getDate());
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			}
		};
	}

	/**
	 * 日历闹铃处理
	 */
	public static Handler alertHandler;

	@Override
	public void onDestroy() {
		try {
			this.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		xmppManager.disconnect();
		executorService.shutdown();
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(LOGTAG, "onBind()...");
		return null;
	}

	public static Intent getIntent() {
		return new Intent(SERVICE_NAME);
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}

	public TaskSubmitter getTaskSubmitter() {
		return taskSubmitter;
	}

	public TaskTracker getTaskTracker() {
		return taskTracker;
	}

	public void connect() {
		if (BackgroundService.isSigned == false) {
			return;
		}

		Log.d(LOGTAG, "connect()...");
		taskSubmitter.submit(new Runnable() {
			@Override
			public void run() {
				xmppManager.connect();
			}
		});
	}

	public void disconnect() {
		// Log.d(LOGTAG, "disconnect()...");
		taskSubmitter.submit(new Runnable() {
			@Override
			public void run() {
				xmppManager.disconnect();
			}
		});
	}

	/**
	 * 提交任务类
	 */
	public class TaskSubmitter {

		final NotificationService notificationService;

		public TaskSubmitter(NotificationService notificationService) {
			this.notificationService = notificationService;
		}

		public Future<?> submit(Runnable task) {
			Future<?> result = null;
			if (!notificationService.getExecutorService().isTerminated()
					&& !notificationService.getExecutorService().isShutdown() && task != null) {
				result = notificationService.getExecutorService().submit(task);
			}
			return result;
		}

	}

	/**
	 * 任务数量监视类
	 */
	public class TaskTracker {

		final NotificationService notificationService;
		public int count;

		public TaskTracker(NotificationService notificationService) {
			this.notificationService = notificationService;
			this.count = 0;
		}

		public void increase() {
			synchronized (notificationService.getTaskTracker()) {
				notificationService.getTaskTracker().count++;
			}
		}

		public void decrease() {
			synchronized (notificationService.getTaskTracker()) {
				notificationService.getTaskTracker().count--;
			}
		}

	}

	/**
	 * 其它用户登录，删除所有数据
	 */
	public static void delAllDb(Context CONTEXT) {
		new FriendHelper(CONTEXT).delete();
		new MsgHelper(CONTEXT).delete();
		new DynamicHelper(CONTEXT).delete();
		new CalendarHelper(CONTEXT).delete();
	}

	/**
	 * 用来判断服务是否运行.
	 * 
	 * @param context
	 * @param className
	 *            判断的服务名字
	 * @return true 在运行 false 不在运行
	 */
	public static boolean isServiceRunning(Context mContext, String className) {
		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) mContext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(30);
		if (!(serviceList.size() > 0)) {
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++) {
			// Log.d("SYS", serviceList.get(i).service.getClassName());
			if (serviceList.get(i).service.getClassName().contains(className)
					&& serviceList.get(i).service.getClassName().contains("scholat")) {
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}

}
