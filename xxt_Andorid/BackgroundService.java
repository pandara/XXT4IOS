package com.scholat.xxt;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;

import com.scholat.xxt.ext.ImageRun;
import com.scholat.xxt.po.Alert;
import com.scholat.xxt.po.Friend;
import com.scholat.xxt.po.User;
import com.scholat.xxt.ui.SearchAdapter;
import com.scholat.xxt.utils.EncryptionDecryption;
import com.scholat.xxt.utils.MediaUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

public class BackgroundService extends Service {
	private static final String LOGTAG = BackgroundService.class.getSimpleName();
	public static final String SERVICE_NAME = "com.scholat.xxt.BackgroundService";

	public static List<Map<String, Object>> msgListData;
	public static List<Map<String, Object>> friendListData;
	public static List<Map<String, Object>> friendDynamicListData;// 好友动态
	public static List<Map<String, Object>> myDynamicListData;// 我的动态
	public static List<Map<String, Object>> calendarListData;
	@SuppressLint("UseSparseArrays")
	public static HashMap<Integer, Alert> alertListData = new HashMap<Integer, Alert>();
	public static HashMap<String, Integer> toReadMap = new HashMap<String, Integer>();
	public static SearchAdapter msgAbstractAdapter;

	// 是否正在闹铃
	public static boolean isAlerting = false;

	// 正在下载apk
	public static boolean isDownloadApk = false;

	// 静态变量
	public static Activity sndActivity;

	public static String usr;
	public static String psw;
	public static String epsw;

	public static boolean firstlog = true;

	// 存储用户信息变量
	public static String mId;
	public static String mUsr;
	public static String mUrl;
	public static String mTitle;
	public static String mChn;

	public static String clientID = "ANON";
	public static String friendMD5 = "MD5";
	public static String calendarMD5 = "MD5";
	public static long mDelta;
	public static Drawable myDrawable = null;
	public static Drawable myRecDrawable = null;
	public static Drawable sndDrawable;

	public static String sndId;
	public static String sndUrl;
	public static String sndUsr;
	public static String sndChn;
	public static String sndTitle;
	public static String sndMsg;
	// public static String sndSdt;
	public static String pshId;
	public static EncryptionDecryption des;

	public static boolean isSigned = false;

	// 消息重传id记录
	public static String chatMsgResponse = "";

	// MainActivity onCreate是否已经执行
	public static boolean isOnCreated = false;

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(LOGTAG, "BackgroundService onBind()...");
		return null;
	}

	public static Intent getIntent() {
		return new Intent(SERVICE_NAME);
	}

	// 返回服务器时间
	public static long getServerTime() {
		return System.currentTimeMillis() + mDelta;
	}

	/**
	 * 重新登录
	 * 
	 */
	public static void reAuth(Context CONTEXT) {
		BackgroundService.isSigned = false;
		try {
			firstlog = false;
			// 授权信息过期，请重新登录！
			Toast toast = Toast.makeText(CONTEXT, CONTEXT.getString(R.string.log_in_again2),
					Toast.LENGTH_LONG);
			toast.show();
			MainActivity.serviceManager.stopService();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			MainActivity.notificatonMgr.cancelAll();
			MainActivity.INSTANCE.finish();
		} catch (Exception e) {

		}

		try {
			Intent intent = new Intent(CONTEXT, LoginActivity.class);
			CONTEXT.startActivity(intent);
		} catch (Exception e) {

		}
		try {
			((Activity) CONTEXT).finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置好友列表头像
	 */
	public static boolean setListIcon(String strUrl, ImageView v) {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String imageUrl = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"
					+ APP_RESOURCE_ROOT + "/" + APP_RESOURCE_AVATAR + "/" + strUrl;
			Bitmap bitmap = MediaUtils.bitmapToRoundCorner(MediaUtils.getBitmapAtLocal(imageUrl));
			if (bitmap != null) {
				v.setImageBitmap(bitmap);
				return true;
			}
		}
		return false;
	}

	/**
	 * 设置好友用户信息
	 */
	public static void setMyFriendInfo(Friend friend, Context CONTEXT) {
		if (friend == null) {
			return;
		}
		sndId = friend.getId();
		sndUsr = friend.getUsr();
		sndChn = friend.getChn();
		sndTitle = friend.getTitle();
		sndUrl = friend.getUrl();

		Drawable mDrawable = MediaUtils.drawableToRoundCorner(MediaUtils
				.getDrawableFormLocal(sndUrl));
		if (mDrawable == null) {
			Log.d(LOGTAG, "setMyFriendInfo 网络获取头像=" + sndUrl);
			mDrawable = MediaUtils.loadImageFromUrl(sndUrl, CONTEXT);

			ImageRun imageRun;
			if (mDrawable != null) {
				Log.d(LOGTAG, "mDrawable != null");
				imageRun = new ImageRun(((BitmapDrawable) mDrawable).getBitmap(), sndUrl);
			} else {
				Log.d(LOGTAG, "mDrawable== null");
				Resources res = CONTEXT.getResources();
				imageRun = new ImageRun(BitmapFactory.decodeResource(res,
						R.drawable.default_icon_rec), sndUrl);
			}
			new Thread(imageRun).start();
			mDrawable = MediaUtils.drawableToRoundCorner(mDrawable);
		}
		sndDrawable = mDrawable;
	}

	/**
	 * 设置登录用户信息
	 */
	public static void setMyUserInfo(User user, Context CONTEXT) {
		mId = user.getId();
		mUsr = user.getUsr();
		mUrl = user.getUrl();
		mTitle = user.getTitle();
		mChn = user.getChn();
		long mTime = Long.parseLong(user.getTime());
		mDelta = mTime - System.currentTimeMillis();
		clientID = Long.toHexString(mTime).toUpperCase(Locale.getDefault()).substring(2);
		// Log.d(LOGTAG, "clientID=" + clientID + " mUrl=" + mUrl);
	}

	// 常量区
	public static String APP_VERSION = "xxtdev";// 当前版本

	public static String NEWAPK_DOWNNAME = "xxtdev.apk";// 新软件下载保存包名
	public static String APP_NEW_VERSION = "xxtdev.apk";// 新软件版本名
	public static int NEWAPK_FILESIZE = 1862;// 新软件安装大小
	public static String DEBUG_MODE = "false";
	public static String SERVER_ADDRESS;
	public static String SCHOLAT_ADDRESS;
	public static int XMPP_PORT;
	public static String XMPP_SERVER;

	public static String APP_RESOURCE_ROOT = "scholat"; // 客户端主目录
	public static String APP_RESOURCE_AVATAR = "avatar"; // 客户端头像主目录
	public static String XMPP_RESOURCE_NAME = "xxt"; // 客户端来源名称
	public static final int REQUEST_TIMEOUT = 4 * 1000;// 设置请求超时4秒钟
	public static final int RESPONSE_TIMEOUT = 5 * 1000; // 设置等待数据超时时间5秒钟
	public static final int SHORT_TIMEOUT = 3 * 1000; // 设置等待数据超时时间3秒钟
	public static SharedPreferences sharedPreference;
	public static final String SETTING_INFOS = "xxtInfo";

	// 操作类型定义
	public static final int TYPE_DYNAMIC_SEND = 1;// 发表动态
	public static final int TYPE_FEEDBACK_SEND = 2;// 发送反馈
	public static final int TYPE_DYNAMIC_MY = 3;// 我的动态
	public static final int TYPE_DYNAMIC_FRIEND = 4;// 好友动态
	public static final int TYPE_USER_LOGOUT = 5; // 退出帐号
	public static final int TYPE_HISTORY_CLEAN = 6;// 删除消息记录

	// 登录类型定义
	public static final int TYPE_AUTO_LOGIN = 1;// 自动登录
	public static final int TYPE_EDIT_LOGIN = 2;// 输入登录

	// 请求返回结果常量定义
	public final static String RESPONSE_NULL = null; // 请求失败
	public final static String RESPONSE_SPACE = ""; // 请求为空
	public final static String RESPONSE_ZERO = "0"; // 请求为空
	public final static String RESPONSE_SENDOK = "1"; // 发送消息成功
	public final static String RESPONSE_ERROR = "000"; // 请求错误
	public final static String RESPONSE_SUCCESS = "010";// 请求成功
	public final static String RESPONSE_SAME = "020";// 服务不变
	public final static String RESPONSE_NONE = "030";// 结果为空
	public final static String RESPONSE_REAUTH = "040";// 授权错误

	public final static String FILE_LOG_NAME = "CRASH-FC.LOG";// 授权错误

	public final static int REAUTH_WHAT = 9999;
	// 屏幕尺寸
	public static int SCREEN_WIDTH = 480;
	public static int SCREEN_HEIGHT = 800;

	// 保存输入框内容，防止误操作丢失
	public static Map<String, String> EditMap = new HashMap<String, String>();

	// 存储小通机器人
	public static String tongId = "12";
	public static String tongUsr = "admin";
	public static String tongUrl = "schol@robot";
	public static String tongTitle = "移动互联网";
	public static String tongChn = "小通机器人";

	// 设置闹钟监听间隔，时间20秒
	public static int ALERT_INTERVAL = 20000;

	// 定时检测XMPP连接状态，时间为5分钟
	public static int XMPP_CHECK_INTERVAL = 300000;
	// XMPP检测定时间隔
	public static int APP_TIMER_INTERVAL = 5000;

	// 定时间隔
	public static int XMPP_TIMER_INTERVAL = 15000;

	// 主程序界面是否正在运行
	public static boolean isMainActivityRunning = false;

	public static Timer tmrXmpp;

	public static long lastXmppCheckingTime = 0;
	// 是否第一次打开动态列表
	public static boolean isFirstReadDynamic = false;
	// 动态列表是否在获取中
	public static boolean isRunDynamic = false;

	// 是否已经认证成功
	public static boolean isAuth = false;
	// 是否正在检测更新
	public static boolean isCheckingUpdate = false;

	// 是否正在聊天
	public static boolean isChatting = false;
	// 聊天内容是否改变
	public static boolean isChatup = false;

	// 是否第一次连接XMPP
	public static boolean isFirstConnectXmpp = true;

	// 是否正在连接XMPP
	public static boolean isConnectingXmpp = false;

	// 日历定时器
	public static Timer alertTimer;
}
