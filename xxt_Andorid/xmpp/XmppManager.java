﻿package com.scholat.xxt.xmpp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.provider.ProviderManager;

import com.scholat.xxt.BackgroundService;
import com.scholat.xxt.MainActivity;
import com.scholat.xxt.NotificationService;
import com.scholat.xxt.NotificationService.TaskSubmitter;
import com.scholat.xxt.NotificationService.TaskTracker;
import com.scholat.xxt.po.Msg;
import com.scholat.xxt.utils.NetUtil;
import android.content.Context;
import android.util.Log;

/**
 * This class is to manage the XMPP connection between client and server.
 * 
 */
public class XmppManager {

	private static final String LOGTAG = XmppManager.class.getSimpleName();

	private Context CONTEXT;
	private TaskSubmitter taskSubmitter;
	private TaskTracker taskTracker;
	private XMPPConnection xmppConnection;

	public XMPPConnection getXmppConnection() {
		return xmppConnection;
	}

	private ConnectionListener connectionListener;
	private PacketListener notificationPacketListener;
	private List<Runnable> taskList;
	private boolean running = false;
	private Future<?> futureTask;

	public XmppManager(NotificationService notificationService) {
		CONTEXT = notificationService;
		taskSubmitter = notificationService.getTaskSubmitter();
		taskTracker = notificationService.getTaskTracker();
		connectionListener = new PersistentConnectionListener();
		notificationPacketListener = new NotificationPacketListener(CONTEXT);
		taskList = new ArrayList<Runnable>();
	}

	public void connect() {
		// Log.d(LOGTAG,"连接服务器");
		submitLoginTask();
	}

	public void disconnect() {
		// Log.d(LOGTAG, "disconnect()");
		terminatePersistentConnection();
	}

	public void terminatePersistentConnection() {
		Runnable runnable = new Runnable() {
			final XmppManager xmppManager = XmppManager.this;

			@Override
			public void run() {
				if (xmppManager.isConnected()) {
					Log.d(LOGTAG, "terminatePersistentConnection()... run()");
					xmppConnection.removePacketListener(xmppManager.notificationPacketListener);
					xmppConnection.disconnect();
				}
				xmppManager.runTask();
			}

		};
		addTask(runnable);
	}

	public void setConnection(XMPPConnection connection) {
		this.xmppConnection = connection;
	}

	public List<Runnable> getTaskList() {
		return taskList;
	}

	public Future<?> getFutureTask() {
		return futureTask;
	}

	public void runTask() {

		if (NetUtil.isNetworkConnected(CONTEXT)) {
			synchronized (taskList) {
				running = false;
				futureTask = null;
				if (!taskList.isEmpty()) {
					Runnable runnable = taskList.get(0);
					taskList.remove(0);
					running = true;
					futureTask = taskSubmitter.submit(runnable);
					if (futureTask == null) {
						taskTracker.decrease();
					}
				}
			}
			taskTracker.decrease();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return xmppConnection != null && xmppConnection.isConnected();
	}

	private boolean isAuthenticated() {
		return xmppConnection != null && xmppConnection.isConnected()
				&& xmppConnection.isAuthenticated();
	}

	private void submitLoginTask() {
		// Log.d(LOGTAG,"提交登录任务()...");
		addTask(new ConnectTask());
	}

	private void addTask(Runnable runnable) {
		// Log.d(LOGTAG,"添加任务()...");
		taskTracker.increase();
		synchronized (taskList) {
			if (taskList.isEmpty() && !running) {
				running = true;
				futureTask = taskSubmitter.submit(runnable);
				if (futureTask == null) {
					taskTracker.decrease();
				}
			} else {
				taskList.add(runnable);
			}
		}
	}

	/**
	 * 连接服务器线程
	 */
	private class ConnectTask implements Runnable {

		final XmppManager xmppManager;

		private ConnectTask() {
			Log.d(LOGTAG, "ConnectTask()...");
			this.xmppManager = XmppManager.this;
		}

		@Override
		public void run() {

			if (!xmppManager.isConnected()) {

				MainActivity.showConnectingHandler.sendEmptyMessage(0);

				ConnectionConfiguration connConfig = new ConnectionConfiguration(
						BackgroundService.XMPP_SERVER, BackgroundService.XMPP_PORT);
				connConfig.setSecurityMode(SecurityMode.required);
				connConfig.setSASLAuthenticationEnabled(false);
				connConfig.setCompressionEnabled(false);

				XMPPConnection connection = new XMPPConnection(connConfig);
				xmppManager.setConnection(connection);

				try {

					connection.connect();
					Log.d(LOGTAG, "XMPP Server Connected");
					ProviderManager.getInstance().addIQProvider(
							XmppConstants.NOTIFICATION_ELEMENTNAME,
							XmppConstants.NOTIFICATION_NTF_NAMESPACE, new NotificationIQProvider());
				} catch (Exception e) {
					Log.d(LOGTAG, "Failed to Connect");
					connect();
				}
				try {
					Thread.sleep(800);
				} catch (Exception e) {
					e.printStackTrace();
				}
				xmppManager.runTask();

			} else {
				addTask(new LoginTask());
				xmppManager.runTask();
			}
		}
	}

	/**
	 * 登录线程
	 */
	private class LoginTask implements Runnable {

		final XmppManager xmppManager;

		private LoginTask() {
			this.xmppManager = XmppManager.this;
		}

		@Override
		public void run() {

			if (!xmppManager.isAuthenticated()) {
				try {
					xmppConnection.login(BackgroundService.mId, BackgroundService.epsw,
							BackgroundService.XMPP_RESOURCE_NAME);
					Log.d(LOGTAG, "LoginTask");
					// connection listener
					if (xmppManager.connectionListener != null) {
						xmppConnection.addConnectionListener(xmppManager.connectionListener);
					}
					PacketFilter packetFilter = new PacketTypeFilter(Msg.class);
					PacketListener packetListener = xmppManager.notificationPacketListener;
					xmppConnection.addPacketListener(packetListener, packetFilter);
					xmppConnection.startKeepAliveThread(xmppManager);

				} catch (Exception e) {
					Log.e(LOGTAG, "Failed to login to xmpp server. Caused by: " + e.getMessage());
					MainActivity.xmmRestartHandler.sendEmptyMessage(0);
				}

			} else {
				MainActivity.xmppProcessHandler.sendEmptyMessage(0);
				Log.d(LOGTAG, "Authenticate Success!");
			}
			xmppManager.runTask();
		}
	}

}
