﻿/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scholat.xxt.xmpp;

import com.scholat.xxt.NotificationService;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * A listener class for monitoring changes in phone connection states.
 * 
 * @author fuchzhou@gmail.com
 */
public class PhoneStateChangeListener extends PhoneStateListener {
	// private static final String LOGTAG =
	// PhoneStateChangeListener.class.getSimpleName();

	@Override
	public void onDataConnectionStateChanged(int state) {
		super.onDataConnectionStateChanged(state);
		// Log.d(LOGTAG,"电话状态" + getState(state));
		if (state == TelephonyManager.DATA_CONNECTED) {
			if (NotificationService.INSTANCE != null) {
				NotificationService.INSTANCE.connect();
			}
		}
	}

}
