﻿/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scholat.xxt.xmpp;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.scholat.xxt.BackgroundService;
import com.scholat.xxt.ChatActivity;
import com.scholat.xxt.MainActivity;
import com.scholat.xxt.R;
import com.scholat.xxt.db.MsgHelper;
import com.scholat.xxt.ext.XmppRun;
import com.scholat.xxt.po.Msg;
import com.scholat.xxt.utils.MediaUtils;

/**
 * 服务器消息推送侦听
 * 
 * @author fuchzhou@gmail.com
 */
public class NotificationPacketListener implements PacketListener {
	private static final String LOGTAG = NotificationPacketListener.class
			.getSimpleName();
	private static Context CONTEXT;

	public NotificationPacketListener(Context CONTEXT) {
		NotificationPacketListener.CONTEXT = CONTEXT;
	}

	@Override
	public void processPacket(Packet packet) {

		if (packet instanceof Msg) {
			Msg msg = (Msg) packet;
			if (msg.getChildElementXML().contains(
					XmppConstants.NOTIFICATION_NTF_NAMESPACE)) {
				NotificationPacketListener.recvmsg(msg);

			}
		}

	}

	private static Notification notification;
	private static PendingIntent pendingIntent;

	/**
	 * 后台提通知的方法
	 */
	public static void tonoticeyou(String tickerText, String frmid,
			String frmchn, String revmsg) {

		notification = new Notification(R.drawable.app_icon, tickerText,
				System.currentTimeMillis());

		notification.flags = Notification.FLAG_NO_CLEAR
				| Notification.FLAG_AUTO_CANCEL;

		Intent in = new Intent(MainActivity.CONTEXT, ChatActivity.class);
		in.putExtra("id", frmid);
		in.putExtra("chn", frmchn);
		in.putExtra("push", true);

		pendingIntent = PendingIntent.getActivity(MainActivity.CONTEXT, 0, in,
				PendingIntent.FLAG_CANCEL_CURRENT);

		notification.setLatestEventInfo(MainActivity.CONTEXT, frmchn, revmsg,
				pendingIntent);

		MainActivity.notificatonMgr.notify(1, notification);

	}

	/**
	 * 接收消息处理
	 */
	public static void recvmsg(Msg msg) {

		try {
			BackgroundService.lastXmppCheckingTime = System.currentTimeMillis();
			String sendTime = msg.getSdt();

			try {
				String sid = Long.toHexString(Long.parseLong(sendTime))
						.substring(7, 10);
				BackgroundService.chatMsgResponse += sid + "@";
				Log.d(LOGTAG, "chatMsgResponse="
						+ BackgroundService.chatMsgResponse);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (new MsgHelper(CONTEXT).isMsgReceived(sendTime)) {
				Log.e(LOGTAG, "Already received!");
				new Thread(new XmppRun(CONTEXT, true)).start();
				return;
			}

			String revMsg = msg.getCon();
			String fromChn = msg.getFch();
			String fromId = msg.getFid();

			if (BackgroundService.chatMsgResponse != null
					&& BackgroundService.chatMsgResponse.split("@").length > 7
					&& XmppRun.isXmppRunning == false) {
				new Thread(new XmppRun(CONTEXT, true)).start();
			}

			if ("12".equals(fromId)) {// 机器人消息
				fromChn = CONTEXT.getString(R.string.robot);
				if ("<DEVING>".equals(revMsg)) {
					revMsg = CONTEXT.getString(R.string.robot_deving);
				}
			}
			if (BackgroundService.sndActivity != null
					&& !BackgroundService.sndActivity.isFinishing()
					&& fromId.equals(BackgroundService.sndId)) {
				// 已经存在本好友对话框，直接追加内容
				ChatActivity.showrecv(revMsg, sendTime);
				// Log.d(LOGTAG, "追加");
				new MsgHelper(CONTEXT).insert(fromId, "1", "1", sendTime,
						revMsg);
			} else {
				BackgroundService.isChatup = true;
				MediaUtils.playSound(0);
				// 原来没有本好友对话框，通知栏提示来消息
				BackgroundService.pshId = fromId;
				// 给您发来消息
				NotificationPacketListener.tonoticeyou(
						fromChn + CONTEXT.getString(R.string.send_you_msg),
						fromId, fromChn, revMsg);
				new MsgHelper(CONTEXT).insert(fromId, "1", "0", sendTime,
						revMsg);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
