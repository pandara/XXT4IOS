﻿/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scholat.xxt.xmpp;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

import com.scholat.xxt.po.Msg;

/**
 * This class parses incoming IQ packets to NotificationIQ objects.
 * 
 * @author fuchzhou@gmail.com
 */
public class NotificationIQProvider implements IQProvider {

	public NotificationIQProvider() {
	}

	@Override
	public IQ parseIQ(XmlPullParser parser) throws Exception {

		Msg msg = new Msg();
		for (boolean done = false; !done;) {
			int eventType = parser.next();
			if (eventType == 2) {
				if ("fid".equals(parser.getName())) {
					msg.setFid(parser.nextText());
				}
				if ("tid".equals(parser.getName())) {
					msg.setTid(parser.nextText());
				}
				if ("fch".equals(parser.getName())) {
					msg.setFch(parser.nextText());
				}
				if ("sdt".equals(parser.getName())) {
					msg.setSdt(parser.nextText());
				}
				if ("con".equals(parser.getName())) {
					msg.setCon(parser.nextText());
				}
			} else if (eventType == 3
					&& XmppConstants.NOTIFICATION_ELEMENTNAME.equals(parser.getName())) {
				done = true;
			}
		}

		return msg;
	}

}
