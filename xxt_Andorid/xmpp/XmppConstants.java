﻿/*
 * Copyright (C) 2010 Moduad Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scholat.xxt.xmpp;

/**
 * 字符串外部化
 * 
 * @author fuchzhou@gmail.com
 */
public class XmppConstants {

	public static final String SHARED_PREFERENCE_NAME = "client_preferences";

	// PREFERENCE KEYS
	public static final String CALLBACK_ACTIVITY_PACKAGE_NAME = "CALLBACK_ACTIVITY_PACKAGE_NAME";
	public static final String CALLBACK_ACTIVITY_CLASS_NAME = "CALLBACK_ACTIVITY_CLASS_NAME";
	public static final String VERSION = "VERSION";
	public static final String XMPP_HOST = "XMPP_HOST";
	public static final String XMPP_PORT = "XMPP_PORT";
	public static final String NOTIFICATION_ICON = "NOTIFICATION_ICON";
	public static final String SETTINGS_NOTIFICATION_ENABLED = "SETTINGS_NOTIFICATION_ENABLED";

	// NOTIFICATION FIELDS
	public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
	public static final String NOTIFICATION_TITLE = "NOTIFICATION_TITLE";
	public static final String NOTIFICATION_MESSAGE = "NOTIFICATION_MESSAGE";

	// INTENT ACTIONS
	public static final String ACTION_SHOW_NOTIFICATION = "com.scholat.xxt.SHOW_NOTIFICATION";
	public static final String ACTION_NOTIFICATION_CLICKED = "com.scholat.xxt.NOTIFICATION_CLICKED";
	public static final String ACTION_NOTIFICATION_CLEARED = "com.scholat.xxt.NOTIFICATION_CLEARED";

	// NOTIFICATION PUSH
	public static final String NOTIFICATION_ROSTER_NAMESPACE = "xxt:iq:roster";
	public static final String NOTIFICATION_AUTH_NAMESPACE = "xxt:iq:auth";
	public static final String NOTIFICATION_NTF_NAMESPACE = "xxt:im:ntf";
	public static final String NOTIFICATION_ELEMENTNAME = "ntf";
	public static final String USERNAME_ELEMENTNAME = "usr";
	public static final String RESOURCE_ELEMENTNAME = "res";
	public static final String DIGEST_ELEMENTNAME = "dig";

}
